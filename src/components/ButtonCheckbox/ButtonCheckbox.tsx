import { FC } from 'react';
import { MainButtonCheckbox } from './styles';
import { TextWrapper } from '../../styles/wrappers';

interface ButtonCheckboxProps {
  title: string;
  titleHidden: boolean;
  width: string;
  height: string;
  backgroundColor: string;
  isBorder?: boolean;
  isMobileCategoryBtn?: boolean;
  borderRadius: string;
  isChecked: boolean;
  onClick: () => void;
}

const ButtonCheckbox: FC<ButtonCheckboxProps> = ({
  title,
  titleHidden,
  width,
  height,
  backgroundColor,
  isBorder,
  isMobileCategoryBtn,
  borderRadius,
  isChecked,
  onClick,
}) => {
  const buttonProps = {
    title,
    width,
    height,
    backgroundColor,
    isBorder,
    isMobileCategoryBtn,
    borderRadius,
    onClick,
    isChecked,
  };
  return (
    <MainButtonCheckbox
      {...buttonProps}
      type='button'
      role='checkbox'
      aria-checked={isChecked}
    >
      {!titleHidden ? (
        <TextWrapper>{title}</TextWrapper>
      ) : (
        <span hidden>{title}</span>
      )}
    </MainButtonCheckbox>
  );
};

export default ButtonCheckbox;
