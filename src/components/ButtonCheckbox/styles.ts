import styled, { css } from 'styled-components';

interface MainButtonCheckboxProps {
  isChecked: boolean;
  width: string;
  height: string;
  backgroundColor: string;
  isBorder?: boolean;
  borderRadius: string;
  isMobileCategoryBtn?: boolean;
}

export const MainButtonCheckbox = styled.button<MainButtonCheckboxProps>`
  all: unset;
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${({ width }) => width};
  height: ${({ height }) => height};
  border: ${({ theme, isBorder }) =>
    isBorder ? `1px solid ${theme.colors.gunmetal}` : ''};
  border-radius: ${({ borderRadius }) => borderRadius};
  background-color: ${({ backgroundColor }) => backgroundColor};
  cursor: pointer;
  ${({ isChecked, isMobileCategoryBtn }) =>
    isChecked &&
    !isMobileCategoryBtn &&
    css`
      outline: ${({ theme }) => `${theme.colors.gunmetal} solid 2px`};
      outline-offset: 6px;
    `}
  ${({ isChecked, isMobileCategoryBtn }) =>
    isChecked &&
    isMobileCategoryBtn &&
    css`
      background-color: ${({ theme }) => theme.colors.gunmetal};
      color: ${({ theme }) => theme.colors.white};
    `}
`;
