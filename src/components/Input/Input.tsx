import { ChangeEvent, FC, memo } from 'react';
import { MainInput, PlaceholderContainer } from './styles';
import { Icon } from '../../assets/icons';
import {
  FlexWrapper,
  PositionWrapper,
  SpacerWrapper,
  TextWrapper,
} from '../../styles/wrappers';
import { useInput } from './useInput';

interface InputProps {
  iconName: string;
  placeholderText?: string;
  maxWidth?: string;
  height?: string;
  value: string | undefined;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  onKeyboardEnterPress?: () => void;
}

const Input: FC<InputProps> = ({
  iconName,
  placeholderText,
  maxWidth,
  height,
  value,
  onChange,
  onKeyboardEnterPress,
}): JSX.Element => {
  const inputProps = { maxWidth, height };
  const {
    inputRef,
    onInputBlur,
    isPlaceholderVisible,
    setInputFocus,
    setIsPlaceholderVisible,
    handleEnterPress,
  } = useInput(value, onKeyboardEnterPress);

  return (
    <PositionWrapper position='relative' width='100%'>
      <MainInput
        ref={inputRef}
        type='text'
        {...inputProps}
        value={value}
        onChange={onChange}
        onFocus={() => setIsPlaceholderVisible(false)}
        onKeyDown={handleEnterPress}
        onBlur={onInputBlur}
      />
      {isPlaceholderVisible ? (
        <PlaceholderContainer onClick={setInputFocus}>
          <FlexWrapper alignItems='center'>
            <Icon iconName={iconName} width={20} height={20} />
            <SpacerWrapper marginLeft='8px'>
              <TextWrapper fontSize='font14' color='cadetGray'>
                {placeholderText}
              </TextWrapper>
            </SpacerWrapper>
          </FlexWrapper>
        </PlaceholderContainer>
      ) : null}
    </PositionWrapper>
  );
};

export default memo(Input);
