import styled from 'styled-components';

interface MainInputProps {
  maxWidth?: string;
  height?: string;
}

export const MainInput = styled.input<MainInputProps>`
  width: 100%;
  max-width: ${({ maxWidth }) => maxWidth};
  height: ${({ height }) => height || '40px'};
  padding: 8px;
  background-color: ${({ theme }) => theme.colors.seasalt};
  border-radius: 8px;
  border: ${({ theme }) => `1px solid ${theme.colors.timberwolf}`};
  transition: 0.25s all;
  &:focus {
    outline: 0 none;
    background-color: ${({ theme }) => theme.colors.white};
    transition: 0.25s all;
  }
`;

export const PlaceholderContainer = styled.div`
  position: absolute;
  top: 50%;
  left: 13px;
  transform: translate(0, -50%);
`;
