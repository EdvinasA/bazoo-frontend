import { useRef, useState, KeyboardEvent, useEffect } from 'react';

export const useInput = (value?: string, keyboardEnterPress?: () => void) => {
  const inputRef = useRef<HTMLInputElement | null>(null);
  const [isPlaceholderVisible, setIsPlaceholderVisible] =
    useState<boolean>(true);
  // focus input
  const setInputFocus = () => {
    if (inputRef.current) inputRef.current.focus();
  };
  // blur input
  const onInputBlur = () => {
    if (inputRef.current && inputRef.current.value.trim().length > 0)
      setIsPlaceholderVisible(false);
    else setIsPlaceholderVisible(true);
  };
  // press enter
  const handleEnterPress = (event: KeyboardEvent<HTMLInputElement>) => {
    if (inputRef.current && event.key === 'Enter') {
      keyboardEnterPress?.(), inputRef.current.blur();
    }
  };
  //---- useEffect ----//
  useEffect(() => {
    if (!value) {
      setIsPlaceholderVisible(true);
    }
    if (value && inputRef.current) setIsPlaceholderVisible(false);
  }, [value]);
  return {
    inputRef,
    onInputBlur,
    isPlaceholderVisible,
    setInputFocus,
    setIsPlaceholderVisible,
    handleEnterPress,
  };
};
