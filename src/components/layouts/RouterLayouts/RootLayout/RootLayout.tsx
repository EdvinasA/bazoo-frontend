import { Outlet } from 'react-router-dom';
import Navigation from '../../../Navigation/Navigation';
import { RootContainer } from './styles';
import Footer from '../../../Footer/Footer';

const HomePageLayout = () => {
  return (
    <>
      <RootContainer>
        <Navigation />
        <Outlet />
      </RootContainer>
      <Footer />
    </>
  );
};

export default HomePageLayout;
