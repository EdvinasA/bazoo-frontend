import { FC } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/opacity.css';

interface ImageProps {
  src: string;
  alt: string;
  width: string;
  height: string;
  style?: React.CSSProperties;
  withEffect: boolean;
}

const Image: FC<ImageProps> = ({
  src,
  alt,
  width,
  height,
  style,
  withEffect,
}): JSX.Element => {
  const imageProps = { src, alt, width, height };
  return (
    <LazyLoadImage
      {...imageProps}
      effect={withEffect ? 'opacity' : undefined}
      style={style}
    />
  );
};

export default Image;
