import styled, { css } from 'styled-components';

interface AccordionButtonProps {
  isActive: boolean;
  background: boolean;
}

export const MainAccordionButton = styled.button<AccordionButtonProps>`
  width: 100%;
  height: 45px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: ${({ theme, isActive, background }) =>
    isActive && background ? theme.colors.antiFlashWhite : 'transparent'};
  padding: 0px 8px;
  border: none;
  font-size: ${({ theme }) => theme.fontSize.base};
  color: ${({ theme }) => theme.colors.charcoal};
  border-radius: 8px;
  cursor: pointer;
`;

interface AccordionButtonIconProps {
  isActive: boolean;
}

export const AccordionButtonIconContainer = styled.div<AccordionButtonIconProps>`
  transition: 0.5s ease-in-out;
  ${({ isActive }) =>
    isActive &&
    css`
      transform: rotate(90deg);
      transition: 0.5s ease-in-out;
    `};
`;
