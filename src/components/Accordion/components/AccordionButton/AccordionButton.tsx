import { Icon, IconNames } from '../../../../assets/icons';
import { TextWrapper } from '../../../../styles/wrappers';
import { AccordionButtonIconContainer, MainAccordionButton } from './styles';
import { ReadonlyProps } from '../../../../types/types';

interface AccordionButtonProps {
  onClick: () => void;
  isActive: boolean;
  background: boolean;
  title: string;
}

type ReadonlyComponentProps = ReadonlyProps<AccordionButtonProps>;

const AccordionButton = (props: ReadonlyComponentProps): JSX.Element => {
  return (
    <MainAccordionButton {...props}>
      <TextWrapper>{props.title}</TextWrapper>
      <AccordionButtonIconContainer isActive={props.isActive}>
        <Icon iconName={IconNames.ArrowRightSmall} width={16} height={16} />
      </AccordionButtonIconContainer>
    </MainAccordionButton>
  );
};

export default AccordionButton;
