import { AccordionContentContainer } from './styles';
import { useAccordion } from './useAccordion';
import AccordionButton from './components/AccordionButton/AccordionButton';
import { ReadonlyProps } from '../../types/types';

interface AccordionProps {
  onClick: () => void;
  isActive: boolean;
  background: boolean;
  title: string;
  children: JSX.Element;
  isOpen: boolean;
}

type ReadonlyComponentProps = ReadonlyProps<AccordionProps>;

const Accordion = (props: ReadonlyComponentProps) => {
  const { accordionRef, accordionHeight } = useAccordion();

  return (
    <>
      <AccordionButton {...props} />
      <AccordionContentContainer
        ref={accordionRef}
        height={props.isOpen ? accordionHeight : 0}
      >
        {props.children}
      </AccordionContentContainer>
    </>
  );
};

export default Accordion;
