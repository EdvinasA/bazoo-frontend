import styled from 'styled-components';

interface AccordionContentContainerProps {
  height: number;
}

export const AccordionContentContainer = styled.div<AccordionContentContainerProps>`
  height: ${({ height }) => `${height}px`};
  transition: height 0.5s ease-in-out;
  overflow: hidden;
`;
