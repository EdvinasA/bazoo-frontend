import { useLayoutEffect, useRef, useState } from 'react';

export const useAccordion = () => {
  const [accordionHeight, setAccordionHeight] = useState<number>(0);
  const accordionRef = useRef<HTMLDivElement | null>(null);

  useLayoutEffect(() => {
    if (accordionRef.current) {
      setAccordionHeight(accordionRef.current.scrollHeight);
    }
  }, [accordionRef.current?.scrollHeight]);

  return {
    accordionRef,
    accordionHeight,
  };
};
