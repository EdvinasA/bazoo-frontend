export enum InputNumberActions {
  Increase = 'Increase',
  Decrease = 'Decrease',
}
