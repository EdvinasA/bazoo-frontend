import { ChangeEvent, FC } from 'react';
import { NumberTypeInputContainer, NumberInput } from './styles';
import Button from '../Button/Button';
import { numberInputButtons } from './data';
import { PositionWrapper } from '../../styles/wrappers';
import { InputNumberActions } from './types';

interface InputNumberProps {
  value: number;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  handleNumberChange: (action: InputNumberActions) => void;
  disabled?: boolean;
}

const InputNumber: FC<InputNumberProps> = ({
  value,
  onChange,
  handleNumberChange,
  disabled,
}): JSX.Element => {
  return (
    <NumberTypeInputContainer>
      <NumberInput
        min={1}
        max={10}
        value={value}
        onChange={onChange}
        onKeyDown={(event) => {
          event.preventDefault();
        }}
      />
      {numberInputButtons.map((item, index) => (
        <PositionWrapper
          key={index}
          position='absolute'
          top='8px'
          left={index === 0 ? '14px' : ''}
          right={index === 1 ? '14px' : ''}
        >
          <Button
            iconSrc={item.icon}
            onClick={() => handleNumberChange(item.action)}
            iconWidth={30}
            disabled={disabled}
          />
        </PositionWrapper>
      ))}
    </NumberTypeInputContainer>
  );
};

export default InputNumber;
