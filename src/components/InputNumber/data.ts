import { IconNames } from '../../assets/icons';
import { InputNumberActions } from './types';

export const numberInputButtons = [
  { icon: IconNames.MinusIcon, action: InputNumberActions.Decrease },
  { icon: IconNames.PlusIcon, action: InputNumberActions.Increase },
];
