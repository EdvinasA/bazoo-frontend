import styled from 'styled-components';

export const NumberTypeInputContainer = styled.div`
  position: relative;
  width: 130px;
`;

export const NumberInput = styled.input.attrs({ type: 'number' })`
  appearance: none;
  display: block;
  width: 130px;
  height: 45px;
  text-align: center;
  border-radius: 8px;
  border: ${({ theme }) => `1px solid ${theme.colors.timberwolf}`};
  font-size: ${({ theme }) => theme.fontSize.font18};
  color: ${({ theme }) => theme.colors.gunmetal};
  font-family: ${({ theme }) => theme.fontFamily.inter};
  outline: none;
  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
  }
  &::-webkit-outer-spin-button {
    -webkit-appearance: none;
  }
`;
