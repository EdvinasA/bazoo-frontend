import styled from 'styled-components';

export const ModalOverlay = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 2;
  transition: opacity 0.3s ease;
  padding: 15px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ModalContainer = styled.div`
  position: relative;
  width: 100%;
  max-width: 550px;
  height: auto;
  border: ${({ theme }) => `1px solid ${theme.colors.darkerAntiFlashWhite}`};
  border-radius: 10px;
  background-color: ${({ theme }) => theme.colors.white};
`;

export const Border = styled.div`
  width: 100%;
  border: ${({ theme }) => `1px solid ${theme.colors.darkerAntiFlashWhite}`};
`;
