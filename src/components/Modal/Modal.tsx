import { FC } from 'react';
import { ModalOverlay, ModalContainer, Border } from './styles';
import Button from '../Button/Button';
import { IconNames } from '../../assets/icons';
import { FlexWrapper, SpacerWrapper } from '../../styles/wrappers';

interface ModalProps {
  isOpen: boolean;
  close: () => void;
  children: JSX.Element;
}

const Modal: FC<ModalProps> = ({ isOpen, close, children }): JSX.Element => {
  return (
    <>
      {isOpen ? (
        <ModalOverlay>
          <ModalContainer>
            <FlexWrapper justifyContent='flex-end' margin='10px'>
              <Button
                width='35px'
                iconSrc={IconNames.CloseIcon}
                iconWidth={35}
                onClick={close}
              />
            </FlexWrapper>
            <Border />
            <SpacerWrapper>{children}</SpacerWrapper>
          </ModalContainer>
        </ModalOverlay>
      ) : null}
    </>
  );
};

export default Modal;
