import { useEffect, useState } from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import { routePath } from '../../../../constants/routes';
import { getCartQuantity } from '../../../../store/cart';
import { getProductFilters } from '../../../../store/products';
import { ButtonActions } from './types';
import { buttons } from './data';
import { searchProducts } from '../../../../store/products/products.actions';

export const useRightNavContent = () => {
  const [searchParams, setSearchParams] = useSearchParams({ q: '' });
  const [isMobilePanelOpen, setIsMobilePanelOpen] = useState<boolean>(false);
  const query = searchParams.get('q');
  const cartQuantity = useAppSelector(getCartQuantity);
  const appliedFilters = useAppSelector(getProductFilters);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  //---- handlers ----//
  const handleButtonClick = (action: string) => {
    switch (action) {
      case ButtonActions.CartAction:
        navigate(routePath.cart);
        break;
      case ButtonActions.UserAction:
        console.log('user action');
        break;
      case ButtonActions.DropdownAction:
        setIsMobilePanelOpen(true);
        break;
    }
  };
  const handleSearch = () => {
    dispatch(searchProducts(query || ''));
    location.pathname !== routePath.catalog
      ? navigate(`${routePath.catalog}?${searchParams}`)
      : location.pathname;
  };

  const getSearchInputValues = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchParams(
      (prev) => {
        prev.set('q', event.target.value);
        return prev;
      },
      { replace: true }
    );
  };
  // remove last button if not a mobile device
  const buttonsData = (isMobile: boolean) => {
    if (!isMobile) {
      return buttons.slice(0, -1);
    }
    return buttons;
  };

  useEffect(() => {
    if (!appliedFilters.category) {
      setSearchParams(
        (prev) => {
          prev.delete('q');
          return prev;
        },
        { replace: true }
      );
    }
  }, [appliedFilters]);

  return {
    query,
    handleButtonClick,
    getSearchInputValues,
    handleSearch,
    cartQuantity,
    buttonsData,
    isMobilePanelOpen,
    setIsMobilePanelOpen,
  };
};
