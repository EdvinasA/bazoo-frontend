import styled from 'styled-components';

interface MobilePanelContainerOverlaProps {
  isOpen: boolean;
}

export const MobilePanelContainerOverlay = styled.div<MobilePanelContainerOverlaProps>`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  display: ${({ isOpen }) => (isOpen ? 'block' : 'none')};
  z-index: 1;
`;

interface MobilePanelContainerProps {
  isOpen: boolean;
}

export const MobilePanelContainer = styled.div<MobilePanelContainerProps>`
  position: fixed;
  top: 0;
  right: 0;
  width: ${({ isOpen }) => (isOpen ? '344px' : '0px')};
  height: 100%;
  background-color: ${({ theme }) => theme.colors.white};
  z-index: 1;
  transition: width 300ms ease-in-out;
  overflow: hidden;
`;

export const CloseButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 318px;
  margin: 6px 15px;
`;
