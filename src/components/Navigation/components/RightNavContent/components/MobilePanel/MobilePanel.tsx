import { FC } from 'react';
import { IconNames } from '../../../../../../assets/icons';
import Button from '../../../../../Button/Button';
import { navData } from '../../../../data';
import {
  MobilePanelContainerOverlay,
  MobilePanelContainer,
  CloseButtonContainer,
} from './styles';
import { SpacerWrapper } from '../../../../../../styles/wrappers';

interface MobilePanelProps {
  isOpen: boolean;
  onClose: () => void;
}

const MobilePanel: FC<MobilePanelProps> = ({
  isOpen,
  onClose,
}): JSX.Element => {
  return (
    <>
      <MobilePanelContainerOverlay isOpen={isOpen} />
      <MobilePanelContainer isOpen={isOpen}>
        <CloseButtonContainer>
          <Button
            iconSrc={IconNames.CloseIcon}
            width='35px'
            iconWidth={35}
            onClick={onClose}
          />
        </CloseButtonContainer>
        {navData.map((item, index) => (
          <SpacerWrapper key={index} padding='15px' width='344px'>
            <Button
              title={item.title}
              titleIconRight={IconNames.ArrowRightSmall}
              itemsBetween
              to={item.routeTo}
              onClick={onClose}
            />
          </SpacerWrapper>
        ))}
      </MobilePanelContainer>
    </>
  );
};

export default MobilePanel;
