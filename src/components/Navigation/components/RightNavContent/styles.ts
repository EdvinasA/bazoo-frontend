import styled from 'styled-components';

export const CartQuantityContainer = styled.div`
  position: absolute;
  top: -15px;
  right: -12px;
  background-color: ${({ theme }) => theme.colors.darkerAntiFlashWhite};
  display: flex;
  justify-content: center;
  align-items: center;
  width: 23px;
  height: 23px;
  border-radius: 50%;
`;
