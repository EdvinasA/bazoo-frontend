export enum ButtonActions {
  CartAction = 'CartAction',
  UserAction = 'UserAction',
  DropdownAction = 'DropdownAction',
}

export interface ButtonsData {
  icon: string;
  action: string;
}
