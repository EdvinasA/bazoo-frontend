import { FC } from 'react';
import { IconNames } from '../../../../assets/icons';
import {
  FlexWrapper,
  PositionWrapper,
  TextWrapper,
} from '../../../../styles/wrappers';
import Button from '../../../Button/Button';
import Input from '../../../Input/Input';
import { useRightNavContent } from './useRightNavContent';
import { CartQuantityContainer } from './styles';
import MobilePanel from './components/MobilePanel/MobilePanel';

interface RightNavContentProps {
  isTablet: boolean;
}

const RightNavContent: FC<RightNavContentProps> = ({
  isTablet,
}): JSX.Element => {
  const {
    query,
    handleButtonClick,
    getSearchInputValues,
    handleSearch,
    cartQuantity,
    buttonsData,
    isMobilePanelOpen,
    setIsMobilePanelOpen,
  } = useRightNavContent();
  return (
    <FlexWrapper width='320px' alignItems='center' justifyContent='flex-end'>
      {!isTablet ? (
        <Input
          value={query ?? ''}
          onChange={getSearchInputValues}
          onKeyboardEnterPress={handleSearch}
          iconName={IconNames.SearchIcon}
          placeholderText='Search'
          maxWidth='250px'
        />
      ) : (
        <MobilePanel
          isOpen={isMobilePanelOpen}
          onClose={() => setIsMobilePanelOpen(false)}
        />
      )}
      {buttonsData(isTablet).map((item, index) => (
        <PositionWrapper
          position='relative'
          key={index}
          marginRight={index === 0 ? '15px' : '0px'}
        >
          <Button
            iconSrc={item.icon}
            iconWidth={27}
            iconHeight={27}
            onClick={() => handleButtonClick(item.action)}
          />
          {index === 0 && cartQuantity > 0 ? (
            <CartQuantityContainer>
              <TextWrapper color='gunmetal' fontSize='font14'>
                {cartQuantity}
              </TextWrapper>
            </CartQuantityContainer>
          ) : null}
        </PositionWrapper>
      ))}
    </FlexWrapper>
  );
};

export default RightNavContent;
