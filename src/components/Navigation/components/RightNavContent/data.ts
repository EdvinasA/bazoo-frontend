import { IconNames } from '../../../../assets/icons';
import { ButtonsData, ButtonActions } from './types';

export const buttons: ButtonsData[] = [
  { icon: IconNames.CartIcon, action: ButtonActions.CartAction },
  // { icon: IconNames.UserIcon, action: ButtonActions.UserAction },
  { icon: IconNames.DropdownButtonIcon, action: ButtonActions.DropdownAction },
];
