import { TextWrapper } from '../../../../styles/wrappers';
import { routePath } from '../../../../constants/routes';
import { NavLink } from './styles';
import { FC } from 'react';
import { ColorsKeys } from '../../../../styles/theme/types';

interface LogoProps {
  color: ColorsKeys;
}

const Logo: FC<LogoProps> = ({ color }): JSX.Element => {
  return (
    <NavLink to={routePath.home}>
      <TextWrapper fontSize='font40' color={color} fontFamily='abril_fatface'>
        Bazoo
      </TextWrapper>
    </NavLink>
  );
};

export default Logo;
