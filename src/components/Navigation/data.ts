import { routePath } from '../../constants/routes';
import { NavigationData } from './types';

export const navData: NavigationData[] = [
  { title: 'Shop', routeTo: routePath.catalog },
  { title: 'Most wanted', routeTo: '#' },
  { title: 'New arrival', routeTo: '#' },
  { title: 'Brands', routeTo: '#' },
];
