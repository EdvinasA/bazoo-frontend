import { useEffect, useState } from 'react';
import { DeviceSizes } from '../../constants/deviceSizes';
import { navData } from './data';

export const useNavigation = () => {
  const [windowWidth, setWindowWidth] = useState<number>(window.innerWidth);
  const [isTabletDevice, setIsTabletDevice] = useState<boolean>(
    windowWidth > DeviceSizes.tablet ? false : true
  );
  // remove last item from array if window width less than 880px
  const naviData = () => {
    if (windowWidth < 880) {
      return navData.slice(0, -1);
    }
    return navData;
  };

  //---- useEffect ----//
  useEffect(() => {
    const handleWindowWidthChange = () => {
      setWindowWidth(window.innerWidth);
    };
    if (windowWidth < DeviceSizes.tablet) {
      setIsTabletDevice(true);
    }
    if (windowWidth > DeviceSizes.tablet) {
      setIsTabletDevice(false);
    }
    window.addEventListener('resize', handleWindowWidthChange);
    return () => {
      window.removeEventListener('resize', handleWindowWidthChange);
    };
  }, [windowWidth]);

  return { isTabletDevice, naviData };
};
