export interface NavigationData {
  title: string;
  routeTo: string;
}
