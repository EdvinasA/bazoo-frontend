import { memo } from 'react';
import { FlexWrapper } from '../../styles/wrappers';
import RightNavContent from './components/RightNavContent/RightNavContent';
import { ListItem, NavLink, NavigationContiner, UnorderedList } from './styles';
import { useNavigation } from './useNavigation';
import Logo from './components/Logo/Logo';

const Navigation = (): JSX.Element => {
  const { isTabletDevice, naviData } = useNavigation();
  return (
    <NavigationContiner>
      <FlexWrapper justifyContent='space-between'>
        <Logo color='gunmetal' />
        {!isTabletDevice ? (
          <UnorderedList>
            {naviData().map((item, index) => (
              <ListItem key={index}>
                <NavLink to={item.routeTo}>{item.title}</NavLink>
              </ListItem>
            ))}
          </UnorderedList>
        ) : null}
        <RightNavContent isTablet={isTabletDevice} />
      </FlexWrapper>
    </NavigationContiner>
  );
};

export default memo(Navigation);
