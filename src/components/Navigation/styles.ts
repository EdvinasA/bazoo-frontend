import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const NavigationContiner = styled.nav`
  padding: 30px 0;
  height: 100px;
  margin-bottom: 35px;
`;

export const UnorderedList = styled.ul`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  gap: 30px;
`;

export const ListItem = styled.li`
  display: inline;
  list-style: none;
`;

export const NavLink = styled(Link)`
  color: ${({ theme }) => theme.colors.gunmetal};
  text-decoration: none;
`;
