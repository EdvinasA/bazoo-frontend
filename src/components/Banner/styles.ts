import styled from 'styled-components';
import { DeviceSizes } from '../../constants/deviceSizes';

export const BannerContainer = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 10px;
  overflow: hidden;
  display: flex;
  flex-wrap: wrap;
  margin: 90px 0px;
`;

export const ImageContainer = styled.div`
  width: 100%;
  max-width: 580px;
  height: 370px;
  @media only screen and (max-width: ${DeviceSizes.desktop}px) {
    max-width: 100%;
    height: 280px;
  }
`;

export const BannerContentContainer = styled.div`
  height: auto;
  background-color: ${({ theme }) => theme.colors.gunmetal};
  flex: 1;
  padding: 55px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  gap: 25px;
`;
