import Image from '../../components/Image/Image';
import {
  BannerContainer,
  ImageContainer,
  BannerContentContainer,
} from './styles';
import { TextWrapper } from '../../styles/wrappers';
import Button from '../../components/Button/Button';
import { IconNames } from '../../assets/icons';

const Banner = (): JSX.Element => {
  return (
    <section>
      <BannerContainer>
        <ImageContainer>
          <Image
            withEffect
            src={
              'https://images.unsplash.com/photo-1487051224065-0a68403c2450?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D'
            }
            alt='ad'
            width='100%'
            height='100%'
            style={{ objectFit: 'cover' }}
          />
        </ImageContainer>
        <BannerContentContainer>
          <TextWrapper color='frenchGrey'>LIMITED OFFER</TextWrapper>
          <TextWrapper fontSize='font40' color='white' fontWeight='bold'>
            35% off only this friday and get special gift
          </TextWrapper>
          <Button
            title='Grab it now'
            titleIconRight={IconNames.ArrowRight}
            width='160px'
            height='52px'
            backgroundColor='white'
          />
        </BannerContentContainer>
      </BannerContainer>
    </section>
  );
};

export default Banner;
