import styled from 'styled-components';
import { DeviceSizes } from '../../constants/deviceSizes';

interface CarouselContainerProps {
  height?: string;
}

export const CarouselContainer = styled.div<CarouselContainerProps>`
  position: relative;
  width: 100%;
  height: ${({ height }) => height || '650px'};
  border-radius: 9px;
  overflow: hidden;
  display: flex;
  @media only screen and (max-width: ${DeviceSizes.laptop}px) {
    height: ${({ height }) => height || '540px'};
  }
  @media only screen and (max-width: ${DeviceSizes.tablet}px) {
    height: ${({ height }) => height || '410px'};
  }
`;

export const Overlay = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  background-color: rgba(105, 94, 82, 0.5);
`;

export const CarouselContent = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 35px;
`;
