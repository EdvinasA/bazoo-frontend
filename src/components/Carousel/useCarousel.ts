import { useEffect, useState } from 'react';
import { ButtonActions } from '../BackForwardButton/types';
import { Images } from './types';

export const useCarousel = (images: Images[]) => {
  const [imgIndex, setImgIndex] = useState<number>(0);

  // handle carousel button click
  const handleButtonClick = (action: ButtonActions) => {
    switch (action) {
      case ButtonActions.Forward:
        setImgIndex((prev) => {
          if (prev === images.length - 1) return 0;
          return prev + 1;
        });
        break;
      case ButtonActions.Backward:
        setImgIndex((prev) => {
          if (prev === 0) return images.length - 1;
          return prev - 1;
        });
        break;
    }
  };

  useEffect(() => {
    if (location.pathname) setImgIndex(0);
  }, [location.pathname]);

  return { imgIndex, handleButtonClick, setImgIndex };
};
