import { FC } from 'react';
import Image from '../Image/Image';
import { CarouselContainer, Overlay, CarouselContent } from './styles';
import { SpacerWrapper, TextWrapper } from '../../styles/wrappers';
import Button from '../Button/Button';
import BackForwardButton from '../BackForwardButton/BackForwardButton';
import { useCarousel } from './useCarousel';
import { Images } from './types';
import CarouselBottomImages from './components/CarouselBottomImages/CarouselBottomImages';
import CarouselDots from './components/CarouselDots/CarouselDots';

interface BackForwardButtonsProps {
  justifyContent: string;
  top: string;
  width: string;
  height: string;
}

interface CarouselProps {
  images: Images[];
  backForwardButtonsProps: BackForwardButtonsProps;
  height?: string;
  text?: string;
  isDots: boolean;
  isButton?: boolean;
  isOverlay?: boolean;
  bottomImages?: boolean;
  buttonTitle?: string;
  titleIcon?: string;
  onClick?: () => void;
}

const Carousel: FC<CarouselProps> = ({
  images,
  backForwardButtonsProps,
  height,
  text,
  isDots,
  isButton,
  isOverlay,
  bottomImages,
  buttonTitle,
  titleIcon,
  onClick,
}): JSX.Element => {
  const { imgIndex, handleButtonClick, setImgIndex } = useCarousel(images);
  return (
    <>
      <CarouselContainer height={height}>
        {images.map((image, index) => (
          <Image
            key={index}
            {...image}
            width='100%'
            height='100%'
            withEffect={false}
            style={{
              objectFit: 'cover',
              translate: `${-100 * imgIndex}%`,
              flexShrink: '0',
              flexGrow: '0',
              transition: 'translate 300ms ease-in-out',
            }}
          />
        ))}
        {isOverlay ? <Overlay /> : null}
        <BackForwardButton
          onClick={(action) => handleButtonClick(action)}
          {...backForwardButtonsProps}
        />
        <CarouselContent>
          {text ? (
            <TextWrapper
              textAlign='center'
              fontSize='font40'
              color='white'
              fontWeight='bold'
            >
              {text}
            </TextWrapper>
          ) : null}
          {isButton ? (
            <Button
              title={buttonTitle}
              titleIconRight={titleIcon}
              onClick={onClick}
              width='150px'
              height='48px'
              backgroundColor='white'
            />
          ) : null}
        </CarouselContent>
        {isDots ? (
          <CarouselDots
            images={images}
            onClick={(index) => setImgIndex(index)}
            currentImage={imgIndex}
          />
        ) : null}
      </CarouselContainer>
      {bottomImages ? (
        <SpacerWrapper marginTop='25px'>
          <CarouselBottomImages
            images={images}
            currentImage={imgIndex}
            onClick={(index) => setImgIndex(index)}
          />
        </SpacerWrapper>
      ) : null}
    </>
  );
};

export default Carousel;
