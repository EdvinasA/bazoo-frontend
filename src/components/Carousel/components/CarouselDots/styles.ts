import styled from 'styled-components';
import { DeviceSizes } from '../../../../constants/deviceSizes';

interface CarouselDotProps {
  isActive: boolean;
}

export const CarouselDot = styled.button<CarouselDotProps>`
  all: unset;
  width: ${({ isActive }) => (isActive ? '10px' : '9px')};
  height: ${({ isActive }) => (isActive ? '10px' : '9px')};
  border-radius: 50%;
  background-color: ${({ isActive, theme }) =>
    isActive ? theme.colors.white : theme.colors.darkerAntiFlashWhite};
  border: ${({ isActive, theme }) =>
    isActive ? `1px solid ${theme.colors.darkerAntiFlashWhite}` : 'none'};
  bottom: 50px;
  left: 50%;
  right: 50%;
  transition: scale 100ms ease-in-out;
  cursor: pointer;
  &:hover {
    scale: 1.2;
  }
  @media only screen and (max-width: ${DeviceSizes.tablet}px) {
    display: none;
  }
`;
