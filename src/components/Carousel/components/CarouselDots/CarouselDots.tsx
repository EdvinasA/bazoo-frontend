import { FC } from 'react';
import { FlexWrapper, PositionWrapper } from '../../../../styles/wrappers';
import { CarouselDot } from './styles';
import { Images } from '../../types';

interface CarouselDotsProps {
  images: Images[];
  currentImage: number;
  onClick: (index: number) => void;
}

const CarouselDots: FC<CarouselDotsProps> = ({
  images,
  currentImage,
  onClick,
}): JSX.Element => {
  return (
    <PositionWrapper
      position='absolute'
      bottom='10rem'
      left='50%'
      transform='-50%'
    >
      <FlexWrapper
        justifyContent='space-between'
        width='100%'
        gap='9px'
        alignItems='center'
      >
        {images.map((_, index) => {
          return (
            <CarouselDot
              key={index}
              onClick={() => onClick(index)}
              isActive={currentImage === index}
            />
          );
        })}
      </FlexWrapper>
    </PositionWrapper>
  );
};

export default CarouselDots;
