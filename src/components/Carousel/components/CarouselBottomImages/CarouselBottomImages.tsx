import { FC } from 'react';
import { Images } from '../../types';
import { FlexWrapper } from '../../../../styles/wrappers';
import Image from '../../../Image/Image';
import { ImageContainer } from './styles';

interface CarouselBottomImagesProps {
  images: Images[];
  currentImage: number;
  onClick: (index: number) => void;
}

const CarouselBottomImages: FC<CarouselBottomImagesProps> = ({
  images,
  currentImage,
  onClick,
}): JSX.Element => {
  return (
    <FlexWrapper gap='15px' flexWrap='wrap'>
      {images.map((image, index) => (
        <ImageContainer
          key={index}
          onClick={() => onClick(index)}
          isActive={currentImage === index}
        >
          <Image
            {...image}
            width='100%'
            height='100%'
            withEffect
            style={{ objectFit: 'cover' }}
          />
        </ImageContainer>
      ))}
    </FlexWrapper>
  );
};

export default CarouselBottomImages;
