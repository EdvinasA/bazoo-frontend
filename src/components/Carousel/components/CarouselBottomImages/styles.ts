import styled from 'styled-components';

interface ImageContainerProps {
  isActive: boolean;
}

export const ImageContainer = styled.div<ImageContainerProps>`
  width: 140px;
  height: 140px;
  border: ${({ isActive, theme }) =>
    isActive ? `2px solid ${theme.colors.gunmetal}` : 'none'};
  border-radius: 5px;
  overflow: hidden;
  cursor: pointer;
`;
