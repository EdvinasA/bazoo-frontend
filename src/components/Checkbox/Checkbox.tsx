import { ChangeEvent, FC } from 'react';
import { MainCheckbox, CheckboxLabel, Checkmark } from './styles';

interface CheckboxProps {
  title: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  isChecked: boolean;
  value: string;
}

const Checkbox: FC<CheckboxProps> = ({
  title,
  onChange,
  isChecked,
  value,
}): JSX.Element => {
  return (
    <CheckboxLabel>
      {title}
      <MainCheckbox checked={isChecked} value={value} onChange={onChange} />
      <Checkmark />
    </CheckboxLabel>
  );
};

export default Checkbox;
