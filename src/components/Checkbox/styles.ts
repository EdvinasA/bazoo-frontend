import styled from 'styled-components';

export const Checkmark = styled.span`
  position: absolute;
  top: 0;
  left: 0;
  height: 19px;
  width: 19px;
  border-radius: 3px;
  border: ${({ theme }) => `1px solid ${theme.colors.gunmetal}`};
  &::after {
    content: '';
    position: absolute;
    display: none;
  }
`;

export const CheckboxLabel = styled.label`
  display: block;
  position: relative;
  padding-left: 27px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: ${({ theme }) => theme.fontSize.base};
  color: ${({ theme }) => theme.colors.gunmetal};
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  &:hover {
    color: ${({ theme }) => theme.colors.cadetGray};
  }
  ${Checkmark}::after {
    left: 5px;
    top: 1px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 2px 2px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }
`;

export const MainCheckbox = styled.input.attrs({ type: 'checkbox' })`
  position: absolute;
  opacity: 0;
  height: 0;
  width: 0;
  cursor: pointer;
  &:checked ~ ${Checkmark} {
    background-color: ${({ theme }) => theme.colors.gunmetal};
  }
  &:checked ~ ${Checkmark}::after {
    display: block;
  }
`;
