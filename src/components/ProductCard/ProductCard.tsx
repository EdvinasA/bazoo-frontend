import { FC } from 'react';
import {
  ProductCardContainer,
  ProductImageContainer,
  SaleBanner,
} from './styles';
import Image from '../Image/Image';
import { TextWrapper } from '../../styles/wrappers';
import FeaturedProductsFooter from './components/FeaturedProductsFooter/FeaturedProductsFooter';
import CatalogFooter from './components/CatalogFooter/CatalogFooter';
import { ProductCardType } from './types';

interface ProductCardProps {
  cardType: ProductCardType;
  image: string;
  title: string;
  price: string;
  oldPrice?: string;
  colors?: number;
  onClick: () => void;
}

const ProductCard: FC<ProductCardProps> = ({
  cardType,
  image,
  title,
  price,
  oldPrice,
  colors,
  onClick,
}): JSX.Element => {
  const catalogFooterProps = { title, price, colors };
  const featuredFooterProps = { title, price, oldPrice, onClick };
  return (
    <ProductCardContainer>
      <ProductImageContainer onClick={onClick}>
        <Image
          src={image}
          alt='product-image'
          withEffect
          width='100%'
          height='100%'
          style={{ objectFit: 'cover' }}
        />
        {oldPrice ? (
          <SaleBanner>
            <TextWrapper fontSize='font14' fontWeight='600' color='white'>
              {'sale'.toUpperCase()}
            </TextWrapper>
          </SaleBanner>
        ) : null}
      </ProductImageContainer>
      {cardType === ProductCardType.Featured ? (
        <FeaturedProductsFooter {...featuredFooterProps} />
      ) : (
        <CatalogFooter {...catalogFooterProps} />
      )}
    </ProductCardContainer>
  );
};

export default ProductCard;
