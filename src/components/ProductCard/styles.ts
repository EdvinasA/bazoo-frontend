import styled from 'styled-components';
import { DeviceSizes } from '../../constants/deviceSizes';

export const ProductCardContainer = styled.div`
  position: relative;
  display: inline-block;
  overflow: hidden;
  width: 100%;
`;

export const ProductImageContainer = styled.div`
  position: relative;
  border-radius: 10px;
  overflow: hidden;
  height: 45vh;
  cursor: pointer;
  @media only screen and (max-width: ${DeviceSizes.desktop}px) {
    height: 40vh;
  }
  @media only screen and (max-width: ${DeviceSizes.mobile}px) {
    height: 50vh;
  }
  @media only screen and (max-height: 600px) {
    height: 80vh;
  }
`;

export const SaleBanner = styled.div`
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  top: 15px;
  left: 15px;
  width: 60px;
  height: 30px;
  background-color: ${({ theme }) => theme.colors.red};
  border-radius: 6px;
  letter-spacing: 1px;
`;
