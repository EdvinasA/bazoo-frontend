import {
  FlexWrapper,
  SpacerWrapper,
  TextWrapper,
} from '../../../../styles/wrappers';
import { Icon, IconNames } from '../../../../assets/icons';
import { ReadonlyProps } from '../../../../types/types';

interface CatalogFooterProps {
  title: string;
  price: string;
  colors?: number;
}

type ReadonlyComponentProps = ReadonlyProps<CatalogFooterProps>;

const CatalogFooter = (props: ReadonlyComponentProps): JSX.Element => {
  return (
    <FlexWrapper justifyContent='space-between' margin='25px 0px 25px 0px'>
      <FlexWrapper flexDirection='column' gap='10px'>
        <TextWrapper color='charcoal'>{props.title}</TextWrapper>
        <TextWrapper color='cadetGray'>{props.colors} Colors</TextWrapper>
      </FlexWrapper>
      <FlexWrapper alignItems='start'>
        <FlexWrapper alignItems='center'>
          <Icon iconName={IconNames.DollarIcon} width={24} fill='charcoal' />
          <SpacerWrapper marginLeft='-5px'>
            <TextWrapper fontSize='font25' fontWeight='600' color='charcoal'>
              {props.price}
            </TextWrapper>
          </SpacerWrapper>
        </FlexWrapper>
      </FlexWrapper>
    </FlexWrapper>
  );
};

export default CatalogFooter;
