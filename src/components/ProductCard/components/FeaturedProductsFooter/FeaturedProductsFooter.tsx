import {
  FlexWrapper,
  SpacerWrapper,
  TextWrapper,
} from '../../../../styles/wrappers';
import { Icon, IconNames } from '../../../../assets/icons';
import Button from '../../../Button/Button';
import { ReadonlyProps } from '../../../../types/types';

interface FeaturedProductsFooterProps {
  title: string;
  price: string;
  oldPrice?: string;
  onClick: () => void;
}

type ReadonlyComponentProps = ReadonlyProps<FeaturedProductsFooterProps>;

const FeaturedProductsFooter = (props: ReadonlyComponentProps): JSX.Element => {
  return (
    <FlexWrapper justifyContent='space-between' margin='25px 0px 25px 0px'>
      <SpacerWrapper>
        <TextWrapper color='charcoal'>{props.title}</TextWrapper>
        <FlexWrapper alignItems='center' marginTop='13px'>
          <Icon iconName={IconNames.DollarIcon} width={25} fill='charcoal' />
          <SpacerWrapper marginLeft='-4px'>
            <TextWrapper fontSize='font30' fontWeight='600' color='charcoal'>
              {props.price}
            </TextWrapper>
          </SpacerWrapper>
          {props.oldPrice ? (
            <FlexWrapper alignItems='center' marginTop='-7px' marginLeft='8px'>
              <Icon
                iconName={IconNames.DollarIcon}
                width={15}
                fill='cadetGray'
              />
              <SpacerWrapper marginLeft='-2px'>
                <TextWrapper color='cadetGray' textDecoration='line-through'>
                  {props.oldPrice}
                </TextWrapper>
              </SpacerWrapper>
            </FlexWrapper>
          ) : null}
        </FlexWrapper>
      </SpacerWrapper>
      <Button
        onClick={props.onClick}
        iconSrc={IconNames.CartPlusWhiteIcon}
        backgroundColor='gunmetal'
        width='54px'
        height='54px'
        iconWidth={28}
        iconHeight={28}
      />
    </FlexWrapper>
  );
};

export default FeaturedProductsFooter;
