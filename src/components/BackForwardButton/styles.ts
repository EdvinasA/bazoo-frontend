import styled from 'styled-components';

interface ButtonsContainerProps {
  top: string;
  justifyContent: string;
}

export const ButtonsContainer = styled.div<ButtonsContainerProps>`
  position: absolute;
  display: flex;
  justify-content: ${({ justifyContent }) => justifyContent};
  width: 100%;
  padding: 0px 30px;
  top: ${({ top }) => top};
  transform: translateY(-50%);
`;
