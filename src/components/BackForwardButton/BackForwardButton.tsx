import { FC } from 'react';
import { buttons } from './data';
import Button from '../Button/Button';
import { ButtonsContainer } from './styles';
import { ButtonActions } from './types';

export interface BackForwardButtonProps {
  justifyContent: string;
  top: string;
  width: string;
  height: string;
  onClick: (action: ButtonActions) => void;
}

const BackForwardButton: FC<BackForwardButtonProps> = ({
  justifyContent,
  top,
  width,
  height,
  onClick,
}): JSX.Element => {
  const buttonsContainerProps = { justifyContent, top };
  const buttonsSizes = { width, height };
  return (
    <ButtonsContainer {...buttonsContainerProps}>
      {buttons.map((item, index) => (
        <Button
          key={index}
          iconSrc={item.icon}
          iconWidth={15}
          iconHeight={15}
          {...buttonsSizes}
          onClick={() => onClick(item.action)}
          backgroundColor='white'
          isBorder
        />
      ))}
    </ButtonsContainer>
  );
};

export default BackForwardButton;
