import { IconNames } from '../../assets/icons';
import { ButtonActions, ButtonData } from './types';

export const buttons: ButtonData[] = [
  { icon: IconNames.ArrowLeftSmall, action: ButtonActions.Backward },
  { icon: IconNames.ArrowRightSmall, action: ButtonActions.Forward },
];
