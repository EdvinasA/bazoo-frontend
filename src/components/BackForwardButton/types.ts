export enum ButtonActions {
  Forward = 'Forward',
  Backward = 'Backward',
}

export interface ButtonData {
  icon: string;
  action: ButtonActions;
}
