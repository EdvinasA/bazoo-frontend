import styled, { css } from 'styled-components';
import { ColorsKeys } from '../../styles/theme/types';
import { Link } from 'react-router-dom';

interface MainButtonProps {
  width?: string;
  height?: string;
  isActive?: boolean;
  isBorder?: boolean;
  isHover?: boolean;
  disabled?: boolean;
  backgroundColor?: ColorsKeys;
  textColor?: ColorsKeys;
  textWeight?: string;
}

export const MainButton = styled.button<MainButtonProps>`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  max-width: ${({ width }) => width};
  height: ${({ height }) => height};
  background-color: ${({ theme, backgroundColor }) =>
    backgroundColor ? theme.colors[backgroundColor] : 'transparent'};
  border: ${({ theme, isBorder }) =>
    isBorder ? `1px solid ${theme.colors.timberwolf}` : 'none'};
  border-radius: 8px;
  color: ${({ theme, textColor }) =>
    (textColor && theme.colors[textColor]) || theme.colors.charcoal};
  font-size: ${({ theme }) => theme.fontSize.font18};
  font-family: ${({ theme }) => theme.fontFamily.inter};
  font-weight: ${({ textWeight }) => textWeight};
  cursor: ${({ disabled }) => (!disabled ? 'pointer' : 'not-allowed')};
  &:hover {
    background-color: ${({ theme, isHover }) =>
      isHover ? theme.colors.gunmetal : ''};
    color: ${({ theme, isHover }) => (isHover ? theme.colors.white : '')};
  }
  ${({ isActive }) =>
    isActive &&
    css`
      background-color: ${({ theme }) => theme.colors.gunmetal};
      color: ${({ theme }) => theme.colors.white};
    `}
`;

export const ButtonLink = styled(Link)`
  all: unset;
`;
