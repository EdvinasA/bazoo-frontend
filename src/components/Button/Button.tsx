import { FC } from 'react';
import { MainButton, ButtonLink } from './styles';
import { Icon } from '../../assets/icons';
import { FlexWrapper, TextWrapper } from '../../styles/wrappers';
import { ColorsKeys } from '../../styles/theme/types';

interface ButtonProps {
  title?: string;
  titleIconLeft?: string;
  titleIconRight?: string;
  itemsBetween?: boolean;
  iconSrc?: string;
  iconWidth?: number;
  iconHeight?: number;
  width?: string;
  height?: string;
  backgroundColor?: ColorsKeys;
  textColor?: ColorsKeys;
  textWeight?: string;
  onClick?: () => void;
  isActive?: boolean;
  isBorder?: boolean;
  isHover?: boolean;
  disabled?: boolean;
  to?: string;
}

const Button: FC<ButtonProps> = ({
  title,
  titleIconLeft,
  titleIconRight,
  itemsBetween,
  iconSrc,
  iconWidth,
  iconHeight,
  onClick,
  isActive,
  isBorder,
  isHover,
  width,
  height,
  backgroundColor,
  textColor,
  textWeight,
  disabled,
  to,
}): JSX.Element => {
  const buttonProps = {
    title,
    onClick,
    isActive,
    isBorder,
    isHover,
    width,
    height,
    backgroundColor,
    textColor,
    textWeight,
    disabled,
  };
  const renderButton = () => {
    return (
      <MainButton {...buttonProps} type='button'>
        {iconSrc ? (
          <Icon iconName={iconSrc} width={iconWidth} height={iconHeight} />
        ) : null}
        {title ? (
          <FlexWrapper
            width='100%'
            justifyContent={itemsBetween ? 'space-between' : 'center'}
            alignItems='center'
            padding={itemsBetween ? '0px 20px' : undefined}
            gap='8px'
          >
            {titleIconLeft ? (
              <Icon iconName={titleIconLeft} width={22} height={22} />
            ) : null}
            <TextWrapper width='inherit'>{title}</TextWrapper>
            {titleIconRight ? (
              <Icon iconName={titleIconRight} width={22} height={22} />
            ) : null}
          </FlexWrapper>
        ) : null}
      </MainButton>
    );
  };
  return (
    <>
      {to ? (
        <ButtonLink to={to}>{renderButton()}</ButtonLink>
      ) : (
        <>{renderButton()}</>
      )}
    </>
  );
};

export default Button;
