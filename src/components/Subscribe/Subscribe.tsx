import Button from '../../components/Button/Button';
import Input from '../../components/Input/Input';
import { FlexWrapper, SpacerWrapper, TextWrapper } from '../../styles/wrappers';
import { IconNames } from '../../assets/icons';
import { InputWithButtonContainer } from './styles';

const Subscribe = (): JSX.Element => {
  return (
    <section>
      <FlexWrapper alignItems='center' flexDirection='column'>
        <TextWrapper
          textAlign='center'
          fontSize='font33'
          fontWeight='600'
          width='700px'
          color='gunmetal'
        >
          Subscribe to our newsletter to get updates to our latest collections
        </TextWrapper>
        <SpacerWrapper margin='25px 0px'>
          <TextWrapper textAlign='center' color='slateGray'>
            Get 20% off on your first order just by subscribing to our
            newsletter
          </TextWrapper>
        </SpacerWrapper>
        <InputWithButtonContainer>
          <Input
            iconName={IconNames.LetterIcon}
            value=''
            placeholderText='Enter your email'
            maxWidth='370px'
            height='50px'
            onChange={() => console.log('on change')}
          />
          <Button
            title='Subscribe'
            width='130px'
            height='50px'
            backgroundColor='gunmetal'
            textColor='white'
          />
        </InputWithButtonContainer>
        <SpacerWrapper marginTop='15px' />
        <TextWrapper
          textAlign='center'
          width='290px'
          color='cadetGray'
          fontSize='font14'
        >
          You will be able to unsubscribe at any time. Read our Privacy Policy
          'here'
        </TextWrapper>
      </FlexWrapper>
    </section>
  );
};

export default Subscribe;
