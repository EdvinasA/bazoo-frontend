import styled from 'styled-components';
import { DeviceSizes } from '../../constants/deviceSizes';

export const InputWithButtonContainer = styled.div`
  width: 100%;
  max-width: 505px;
  gap: 5px;
  display: flex;
  @media only screen and (max-width: ${DeviceSizes.mobile}px) {
    flex-wrap: wrap;
    gap: 12px;
  }
`;
