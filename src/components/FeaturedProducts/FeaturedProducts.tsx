import { memo } from 'react';
import Heading from '../../components/Heading/Heading';
import { useFeaturedProducts } from './useFeaturedProducts';
import ProductCard from '../../components/ProductCard/ProductCard';
import { CarouselContainer, FeaturedProductContainer } from './styles';
import { FlexWrapper } from '../../styles/wrappers';
import { ProductCardType } from '../../components/ProductCard/types';
import { ReadonlyProps } from '../../types/types';

interface FeaturedProductsProps {
  title: string;
}

type ReadonlyComponentProps = ReadonlyProps<FeaturedProductsProps>;

const FeaturedProducts = (props: ReadonlyComponentProps): JSX.Element => {
  const { featuredProducts, handleButtonClick } = useFeaturedProducts();
  return (
    <section>
      <FlexWrapper
        justifyContent='space-between'
        alignItems='center'
        margin='80px 0px 40px 0px'
      >
        <Heading {...props} />
      </FlexWrapper>
      <CarouselContainer>
        {featuredProducts?.map((item, index) => (
          <FeaturedProductContainer key={index}>
            <ProductCard
              cardType={ProductCardType.Featured}
              image={item.images[0].src}
              title={item.title}
              price={item.price}
              oldPrice={item.oldPrice}
              onClick={() => handleButtonClick(item)}
            />
          </FeaturedProductContainer>
        ))}
      </CarouselContainer>
    </section>
  );
};

export default memo(FeaturedProducts);
