import styled from 'styled-components';
import { DeviceSizes } from '../../constants/deviceSizes';

export const CarouselContainer = styled.div`
  width: 100%;
  height: 100%;
  margin: 0 auto;
  overflow: hidden;
  white-space: nowrap;
  scroll-behavior: smooth;
  overflow-x: scroll;
  ::-webkit-scrollbar {
    height: 7px;
  }
  /* Track */
  ::-webkit-scrollbar-track {
    background-color: #ebeef2;
    border-radius: 6px;
  }
  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: ${({ theme }) => theme.colors.gunmetal};
    border-radius: 6px;
  }
  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: ${({ theme }) => theme.colors.gunmetal};
  }
`;

export const FeaturedProductContainer = styled.div`
  display: inline-block;
  width: 100%;
  max-width: 20.5vw;
  margin: 10px;
  @media only screen and (max-width: ${DeviceSizes.desktop}px) {
    max-width: 30vw;
  }
  @media only screen and (max-width: ${DeviceSizes.laptop}px) {
    max-width: 45vw;
  }
  @media only screen and (max-width: ${DeviceSizes.tablet}px) {
    max-width: 43.5vw;
  }
  @media only screen and (max-width: ${DeviceSizes.mobile}px) {
    max-width: 84vw;
  }
`;
