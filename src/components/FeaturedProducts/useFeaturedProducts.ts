import { useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { getFeaturedProduct } from '../../store/products';
import { Products } from '../../store/products/types';
import { useEffect } from 'react';
import { getFeaturedProducts } from '../../store/products/products.actions';

export const useFeaturedProducts = () => {
  const featuredProducts = useAppSelector(getFeaturedProduct);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  //---- handler ----//
  const handleButtonClick = (item: Products) => {
    navigate(`/product/${item.slug}`, { replace: true });
  };

  //---- useEffect ----//
  useEffect(() => {
    if (!featuredProducts?.length) dispatch(getFeaturedProducts());
  }, [featuredProducts]);
  return { featuredProducts, handleButtonClick };
};
