import styled from 'styled-components';

interface MainHeadingProps {
  margin?: string;
}

export const MainHeading = styled.h2<MainHeadingProps>`
  margin: ${({ margin }) => margin};
  font-size: ${({ theme }) => theme.fontSize.font33};
  font-weight: 600;
  color: ${({ theme }) => theme.colors.gunmetal};
`;
