import { FC } from 'react';
import { MainHeading } from './styles';

interface HeadingProps {
  title: string;
  margin?: string;
}

const Heading: FC<HeadingProps> = ({ title, margin }): JSX.Element => {
  return <MainHeading margin={margin}>{title}</MainHeading>;
};
export default Heading;
