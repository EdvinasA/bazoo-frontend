interface FooterIcons {
  icon: string;
}

interface Footerlinks {
  title: string;
  link: string;
}

export interface FooterData {
  title: string;
  links?: Footerlinks[];
  icons?: FooterIcons[];
}
