import { Icon } from '../../assets/icons';
import { FlexWrapper, SpacerWrapper, TextWrapper } from '../../styles/wrappers';
import Logo from '../Navigation/components/Logo/Logo';
import { RootContainer } from '../layouts/RouterLayouts/RootLayout/styles';
import { footerData } from './data';
import {
  FooterContainer,
  FooterContentContainer,
  FooterLinksContainer,
  FooterLinks,
  UnorderedList,
  UnorderedListItem,
  Border,
} from './style';

const Footer = (): JSX.Element => {
  const date = new Date();
  return (
    <FooterContainer>
      <RootContainer>
        <FooterContentContainer>
          <SpacerWrapper marginBottom='55px' width='400px'>
            <Logo color='charcoal' />
            <SpacerWrapper marginBottom='15px' />
            <TextWrapper lineHeight='lh26' color='cadetGray'>
              Specializes in providing high-quality, stylish products for your
              wardrobe
            </TextWrapper>
          </SpacerWrapper>
          <FooterLinksContainer>
            {footerData.map((data, index, { length }) => (
              <FooterLinks key={index}>
                <TextWrapper
                  fontWeight='600'
                  color='charcoal'
                  fontSize='font18'
                >
                  {data.title.toUpperCase()}
                </TextWrapper>
                <SpacerWrapper marginBottom='15px' />
                <UnorderedList>
                  {data.links?.map((link, index) => (
                    <UnorderedListItem key={index}>
                      {link.title}
                    </UnorderedListItem>
                  ))}
                  {length - 1 === index && (
                    <FlexWrapper justifyContent='space-between'>
                      {data.icons?.map((item, index) => (
                        <Icon
                          key={index}
                          iconName={item.icon}
                          width={index !== 0 ? 70 : 40}
                          height={40}
                        />
                      ))}
                    </FlexWrapper>
                  )}
                </UnorderedList>
              </FooterLinks>
            ))}
          </FooterLinksContainer>
        </FooterContentContainer>
        <Border />
        <TextWrapper textAlign='center' color='cadetGray'>
          Copyright &copy; {date.getFullYear()} Bazoo. All rights reserved
        </TextWrapper>
      </RootContainer>
    </FooterContainer>
  );
};

export default Footer;
