import styled from 'styled-components';
import { DeviceSizes } from '../../constants/deviceSizes';

export const FooterContainer = styled.footer`
  width: 100%;
  background-color: ${({ theme }) => theme.colors.darkerAntiFlashWhite};
  margin-top: 120px;
  padding: 50px 0px 30px 0px;
`;

export const FooterContentContainer = styled.div`
  display: flex;
  justify-content: space-between;
  @media only screen and (max-width: ${DeviceSizes.tablet}px) {
    flex-wrap: wrap;
  }
`;

export const FooterLinksContainer = styled.div`
  width: 100%;
  max-width: 750px;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  padding-left: 40px;
  @media only screen and (max-width: ${DeviceSizes.laptop}px) {
    max-width: 50%;
  }
  @media only screen and (max-width: ${DeviceSizes.tablet}px) {
    padding-left: 0px;
    flex-wrap: nowrap;
    max-width: 100%;
  }
  @media only screen and (max-width: ${DeviceSizes.mobile}px) {
    flex-wrap: wrap;
  }
`;

export const FooterLinks = styled.div`
  height: 150px;
  margin-bottom: 20px;
  @media only screen and (max-width: ${DeviceSizes.laptop}px) {
    width: 48%;
  }
`;

export const UnorderedList = styled.ul`
  padding: 0;
  list-style: none;
`;

export const UnorderedListItem = styled.li`
  display: flex;
  height: 35px;
  align-items: center;
  color: ${({ theme }) => theme.colors.cadetGray};
`;

export const Border = styled.div`
  width: 100%;
  height: 1px;
  border: ${({ theme }) => `1px solid ${theme.colors.platinum}`};
  margin: 30px 0px 30px 0px;
`;
