import { IconNames } from '../../assets/icons';
import { FooterData } from './types';

export const footerData: FooterData[] = [
  {
    title: 'shop',
    links: [
      { title: 'All Collections', link: '' },
      { title: 'Winter Edition', link: '' },
      { title: 'Discount', link: '' },
    ],
  },
  {
    title: 'company',
    links: [
      { title: 'About Us', link: '' },
      { title: 'Contact', link: '' },
      { title: 'Affiliates', link: '' },
    ],
  },
  {
    title: 'support',
    links: [
      { title: `FAQ's`, link: '' },
      { title: 'Cookie Policy', link: '' },
      { title: 'Terms of Use', link: '' },
    ],
  },
  {
    title: 'payment methods',
    icons: [
      { icon: IconNames.MastercardLogo },
      { icon: IconNames.VisaLogo },
      { icon: IconNames.PaypalLogo },
    ],
  },
];
