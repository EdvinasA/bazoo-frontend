export const apiEndpoint = {
  getAllProducts: () => '/products/all',
  getSingleProduct: (slug: string) => `/products/${slug}`,
  getFeaturedProducts: () => `/products/featured`,
  getFilteredProducts: () => `/products/filtered`,
  searchProducts: () => `/products/search`,
};
