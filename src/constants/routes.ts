export const routePath = {
  home: '/',
  about: '/about',
  catalog: '/catalog',
  product: '/product/:slug',
  cart: '/cart',
};
