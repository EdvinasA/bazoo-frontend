export const DeviceSizes = {
  mobile: 480,
  tablet: 768,
  laptop: 1024,
  desktop: 1200,
};
