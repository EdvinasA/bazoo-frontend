import { FC } from 'react';
import Delivery from './delivery.svg?react';
import Mask from './mask-happy.svg?react';
import MoneySend from './money-send.svg?react';
import Truck from './truck-fast.svg?react';

export enum GuaranteeIconNames {
  Delivery = 'Delivery',
  Mask = 'Mask',
  MoneySend = 'MoneySend',
  Truck = 'Truck',
}

interface GuaranteeIconsProps {
  width?: number;
  height?: number;
  iconName: string;
}

export const GuaranteeIcons: FC<GuaranteeIconsProps> = ({
  iconName,
  width,
  height,
}): JSX.Element | null => {
  switch (iconName) {
    case GuaranteeIconNames.Delivery:
      return <Delivery width={width} height={height} />;
    case GuaranteeIconNames.Mask:
      return <Mask width={width} height={height} />;
    case GuaranteeIconNames.MoneySend:
      return <MoneySend width={width} height={height} />;
    case GuaranteeIconNames.Truck:
      return <Truck width={width} height={height} />;
    default:
      return <></>;
  }
};
