import { FC } from 'react';
import MastercardLogo from './Mastercard-Logo.svg?react';
import PaypalLogo from './paypal-3.svg?react';
import VisaLogo from './Visa_Inc.-Logo.svg?react';
import SearchIcon from './search-icon.svg?react';
import CartIcon from './cart-icon.svg?react';
import UserIcon from './user-icon.svg?react';
import ArrowRight from './arrow-right2.svg?react';
import ArrowLeftSmall from './arrow_left_small_icon.svg?react';
import ArrowRightSmall from './arrow_right_small_icon.svg?react';
import LetterIcon from './letter.svg?react';
import CartPlusWhiteIcon from './cart-plus-white.svg?react';
import DollarIcon from './dollar-icon.svg?react';
import CloseIcon from './close-icon.svg?react';
import PlusIcon from './plus-icon.svg?react';
import MinusIcon from './minus-icon.svg?react';
import TrashIcon from './trash-icon.svg?react';
import FilterOptionsIcon from './filter-options-icon.svg?react';
import CloseCircleIcon from './close-circle-icon.svg?react';
import DropdownButtonIcon from './dropdown-button-black.svg?react';
import { ColorsKeys } from '../../styles/theme/types';
import { theme } from '../../styles/theme/theme';

export enum IconNames {
  MastercardLogo = 'MastercardLogo',
  PaypalLogo = 'PaypalLogo',
  VisaLogo = 'VisaLogo',
  SearchIcon = 'SearchIcon',
  CartIcon = 'CartIcon',
  UserIcon = 'UserIcon',
  ArrowRight = 'ArrowRight',
  ArrowLeftSmall = 'ArrowLeftSmall',
  ArrowRightSmall = 'ArrowRightSmall',
  LetterIcon = 'LetterIcon',
  CartPlusWhiteIcon = 'CartPlusWhiteIcon',
  DollarIcon = 'DollarIcon',
  CloseIcon = 'CloseIcon',
  PlusIcon = 'PlusIcon',
  MinusIcon = 'MinusIcon',
  TrashIcon = 'TrashIcon',
  FilterOptionsIcon = 'FilterOptionsIcon',
  CloseCircleIcon = 'CloseCircleIcon',
  DropdownButtonIcon = 'DropdownButtonIcon',
}

interface IconProps {
  iconName: string;
  width?: number;
  height?: number;
  fill?: ColorsKeys;
}

export const Icon: FC<IconProps> = ({
  iconName,
  width,
  height,
  fill,
}): JSX.Element | null => {
  switch (iconName) {
    case IconNames.MastercardLogo:
      return <MastercardLogo width={width} height={height} />;
    case IconNames.PaypalLogo:
      return <PaypalLogo width={width} height={height} />;
    case IconNames.VisaLogo:
      return <VisaLogo width={width} height={height} />;
    case IconNames.SearchIcon:
      return <SearchIcon width={width} height={height} />;
    case IconNames.CartIcon:
      return <CartIcon width={width} height={height} />;
    case IconNames.UserIcon:
      return <UserIcon width={width} height={height} />;
    case IconNames.ArrowRight:
      return <ArrowRight width={width} height={height} />;
    case IconNames.ArrowLeftSmall:
      return <ArrowLeftSmall width={width} height={height} />;
    case IconNames.ArrowRightSmall:
      return <ArrowRightSmall width={width} height={height} />;
    case IconNames.LetterIcon:
      return <LetterIcon width={width} height={height} />;
    case IconNames.CartPlusWhiteIcon:
      return <CartPlusWhiteIcon width={width} height={height} />;
    case IconNames.DollarIcon:
      return (
        <DollarIcon
          width={width}
          height={height}
          fill={fill && theme.colors[fill]}
        />
      );
    case IconNames.CloseIcon:
      return <CloseIcon width={width} height={height} />;
    case IconNames.PlusIcon:
      return <PlusIcon width={width} height={height} />;
    case IconNames.MinusIcon:
      return <MinusIcon width={width} height={height} />;
    case IconNames.TrashIcon:
      return <TrashIcon width={width} height={height} />;
    case IconNames.FilterOptionsIcon:
      return <FilterOptionsIcon width={width} height={height} />;
    case IconNames.CloseCircleIcon:
      return <CloseCircleIcon width={width} height={height} />;
    case IconNames.DropdownButtonIcon:
      return <DropdownButtonIcon width={width} height={height} />;
    default:
      return <></>;
  }
};
