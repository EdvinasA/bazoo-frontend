import { FC } from 'react';
import Adidas from './adidas.svg?react';
import Nike from './nike-8-1.svg?react';
import Chanel from './chanel.svg?react';
//import DolceGabbana from './dolce-gabbana.svg?react';
import Gucci from './gucci.svg?react';
import GuesJeans from './guess-jeans.svg?react';
import Levis from './levis.svg?react';
import Versace from './versace-medusa.svg?react';
import VansLogo from './vans-off-the-wall-palm-tree.svg?react';
//https://www.svgrepo.com/

export enum BrandIconNames {
  Adidas = 'Adidas',
  Nike = 'Nike',
  Chanel = 'Chanel',
  VansLogo = 'VansLogo',
  Gucci = 'Gucci',
  GuesJeans = 'GuesJeans',
  Levis = 'Levis',
  Versace = 'Versace',
}

interface BrandsIconsProps {
  width?: number;
  height?: number;
  iconName: string;
}

export const BrandsIcons: FC<BrandsIconsProps> = ({
  iconName,
  width,
  height,
}): JSX.Element | null => {
  switch (iconName) {
    case BrandIconNames.Adidas:
      return <Adidas width={width} height={height} />;
    case BrandIconNames.Nike:
      return <Nike width={width} height={height} />;
    case BrandIconNames.Chanel:
      return <Chanel width={width} height={height} />;
    case BrandIconNames.VansLogo:
      return <VansLogo width={width} height={height} />;
    case BrandIconNames.Gucci:
      return <Gucci width={width} height={height} />;
    case BrandIconNames.GuesJeans:
      return <GuesJeans width={width} height={height} />;
    case BrandIconNames.Levis:
      return <Levis width={width} height={height} />;
    case BrandIconNames.Versace:
      return <Versace width={width} height={height} />;
    default:
      return <></>;
  }
};
