import styled from 'styled-components';
import {
  ColorsKeys,
  FontFamilyKeys,
  FontSizeKeys,
  LineHeightKeys,
} from './theme/types';

export interface SpacerWrapperProps {
  margin?: string;
  marginTop?: string;
  marginLeft?: string;
  marginRight?: string;
  marginBottom?: string;
  padding?: string;
  width?: string;
  height?: string;
}

export const SpacerWrapper = styled.div<SpacerWrapperProps>`
  margin: ${({ margin }) => margin};
  margin-top: ${({ marginTop }) => marginTop};
  margin-left: ${({ marginLeft }) => marginLeft};
  margin-right: ${({ marginRight }) => marginRight};
  margin-bottom: ${({ marginBottom }) => marginBottom};
  padding: ${({ padding }) => padding};
  width: ${({ width }) => width};
  height: ${({ height }) => height};
`;

export interface TextWrapperProps {
  fontSize?: FontSizeKeys;
  fontFamily?: FontFamilyKeys;
  color?: ColorsKeys;
  lineHeight?: LineHeightKeys;
  fontStyle?: string;
  fontWeight?: string;
  width?: string;
  textAlign?: string;
  textDecoration?: string;
}

export const TextWrapper = styled.p<TextWrapperProps>`
  max-width: ${({ width }) => width};
  font-family: ${({ theme, fontFamily }) =>
    fontFamily && theme.fontFamily[fontFamily]};
  font-size: ${({ theme, fontSize }) => fontSize && theme.fontSize[fontSize]};
  font-style: ${({ fontStyle }) => fontStyle};
  font-weight: ${({ fontWeight }) => fontWeight};
  color: ${({ theme, color }) => color && theme.colors[color]};
  line-height: ${({ theme, lineHeight }) =>
    lineHeight && theme.lineHeight[lineHeight]};
  text-align: ${({ textAlign }) => textAlign};
  text-decoration: ${({ textDecoration }) => textDecoration};
`;

interface FlexWrapperProps {
  alignItems?: string;
  justifyContent?: string;
  flexDirection?: string;
  flexWrap?: string;
  gap?: string;
}

export const FlexWrapper = styled(SpacerWrapper)<FlexWrapperProps>`
  display: flex;
  align-items: ${({ alignItems }) => alignItems};
  justify-content: ${({ justifyContent }) => justifyContent};
  flex-direction: ${({ flexDirection }) => flexDirection};
  flex-wrap: ${({ flexWrap }) => flexWrap};
  gap: ${({ gap }) => gap};
`;

interface PositionWrapperProps {
  position?: 'relative' | 'absolute' | 'fixed';
  top?: string;
  bottom?: string;
  left?: string;
  right?: string;
  zIndex?: number;
  transform?: string;
}

export const PositionWrapper = styled(SpacerWrapper)<PositionWrapperProps>`
  position: ${({ position }) => position};
  top: ${({ top }) => top};
  bottom: ${({ bottom }) => bottom};
  left: ${({ left }) => left};
  right: ${({ right }) => right};
  z-index: ${({ zIndex }) => zIndex};
  transform: ${({ transform }) => `translate(${transform})`};
`;
