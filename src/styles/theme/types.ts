import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    colors: Colors;
    fontFamily: FontFamily;
    fontSize: FontSize;
    lineHeight: LineHeight;
  }
}

interface Colors {
  red: string;
  snow: string;
  antiFlashWhite: string;
  darkerAntiFlashWhite: string;
  white: string;
  gunmetal: string;
  timberwolf: string;
  platinum: string;
  seasalt: string;
  frenchGrey: string;
  cadetGray: string;
  slateGray: string;
  charcoal: string;
}

interface FontFamily {
  inter: string;
  //logo
  abril_fatface: string;
}

interface FontSize {
  base: string;
  font11: string;
  font14: string;
  font18: string;
  font20: string;
  font23: string;
  font25: string;
  font30: string;
  font33: string;
  font40: string;
}

interface LineHeight {
  lh26: string;
  lh28: string;
  lh65: string;
}

export type ColorsKeys = keyof Colors;
export type FontFamilyKeys = keyof FontFamily;
export type FontSizeKeys = keyof FontSize;
export type LineHeightKeys = keyof LineHeight;
