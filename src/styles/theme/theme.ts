import { DefaultTheme } from 'styled-components';

export const theme: DefaultTheme = {
  colors: {
    red: '#CC2000',
    snow: '#F9F5F6',
    antiFlashWhite: '#F5F7F9',
    darkerAntiFlashWhite: '#EBEEF2',
    white: '#FFFFFF',
    gunmetal: '#1D242D',
    timberwolf: '#D1D1D1',
    platinum: '#D4D9E0',
    seasalt: '#F9FAFB',
    frenchGrey: '#B2BCC6',
    cadetGray: '#909DAD',
    slateGray: '#697B91',
    charcoal: '#3D4C5E',
  },
  fontFamily: {
    inter: '"Inter", sans-serif;',
    abril_fatface: '"Abril Fatface", serif',
  },
  fontSize: {
    base: '1rem',
    font11: '0.688rem',
    font14: '0.875rem',
    font18: '1.125rem',
    font20: '1.25rem',
    font23: '1.438rem',
    font25: '1.563rem',
    font30: '1.875rem',
    font33: '2.063rem',
    font40: '2.5rem',
  },
  lineHeight: {
    lh26: '26px',
    lh28: '28px',
    lh65: '65px',
  },
};
