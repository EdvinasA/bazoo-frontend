import {
  Route,
  createBrowserRouter,
  createRoutesFromElements,
} from 'react-router-dom';
import RootLayout from '../components/layouts/RouterLayouts/RootLayout/RootLayout';
import { routePath } from '../constants/routes';
import Home from '../pages/Home/Home';
import About from '../pages/About/About';
import Catalog from '../pages/Catalog/Catalog';
import Product from '../pages/Product/Product';
import Cart from '../pages/Cart/Cart';

export const router = createBrowserRouter(
  createRoutesFromElements(
    <Route element={<RootLayout />}>
      <Route index path={routePath.home} element={<Home />} />
      <Route path={routePath.about} element={<About />} />
      <Route path={routePath.catalog} element={<Catalog />} />
      <Route path={routePath.product} element={<Product />} />
      <Route path={routePath.cart} element={<Cart />} />
    </Route>
  )
);
