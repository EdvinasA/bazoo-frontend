import { all } from 'redux-saga/effects';
import productsSaga from './products/products.sagas';

export default function* rootSaga() {
  yield all([productsSaga()]);
}
