import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { Cart, CartProduct } from './types';
import { InputNumberActions } from '../../components/InputNumber/types';

export interface CartState {
  cart: Cart;
  quantity: number;
  isModalOpen: boolean;
}

const initialState: CartState = {
  cart: { products: [], totalPrice: 0 },
  quantity: 0,
  isModalOpen: false,
};

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    // add to cart
    addToCart: (state, action: PayloadAction<CartProduct>) => {
      state.cart.products = [...state.cart.products, action.payload];
      state.isModalOpen = true;
    },

    // update product quantity
    updateProductQuantity: (
      state,
      action: PayloadAction<{ index: number; action: InputNumberActions }>
    ) => {
      const { cart } = state;
      switch (action.payload.action) {
        case InputNumberActions.Increase:
          if (
            cart.products[action.payload.index].quantity <
            cart.products[action.payload.index].productCount
          )
            cart.products[action.payload.index].quantity += 1;
          break;
        case InputNumberActions.Decrease:
          if (cart.products[action.payload.index].quantity !== 1)
            cart.products[action.payload.index].quantity -= 1;
          break;
        default:
          cart.products[action.payload.index].quantity += 1;
      }
    },
    // update total price and quantity
    updateTotalPriceAndQuantity: (state) => {
      const { cart } = state;
      state.quantity = cart.products.reduce(
        (amount, item) => amount + item.quantity,
        0
      );
      state.cart.totalPrice = cart.products.reduce(
        (total, item) => total + Number(item.price) * item.quantity,
        0
      );
    },
    // delete product from cart
    deleteProductFromCart: (state, action: PayloadAction<number>) => {
      const { cart } = state;
      const product = cart.products.findIndex(
        (_, index) => index === action.payload
      );
      cart.products.splice(product, 1);
    },
    // delete all products from cart
    deleteAllProducts: () => {
      return initialState;
    },
    // modal when product is added to the cart
    setIsCartMessageModalOpen: (state, action: PayloadAction<boolean>) => {
      state.isModalOpen = action.payload;
    },
  },
});

export const {
  addToCart,
  updateProductQuantity,
  updateTotalPriceAndQuantity,
  deleteProductFromCart,
  deleteAllProducts,
  setIsCartMessageModalOpen,
} = cartSlice.actions;
export default cartSlice.reducer;
