export interface CartProduct {
  title: string;
  image: string;
  color: string;
  size: string;
  productCount: number;
  quantity: number;
  price: string;
  slug: string;
}

export interface Cart {
  products: CartProduct[];
  totalPrice: number;
}
