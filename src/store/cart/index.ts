import { CartProduct } from './types';

export const getCartProducts = (state: RootState): CartProduct[] =>
  state.cart.cart.products;

export const getCartQuantity = (state: RootState): number =>
  state.cart.quantity;

export const getTotalPrice = (state: RootState): number =>
  state.cart.cart.totalPrice;

export const isCartMessageModalOpen = (state: RootState): boolean =>
  state.cart.isModalOpen;
