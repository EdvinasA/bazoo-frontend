import { Products, ProductsFilter } from './types';

//get all products
export const getProducts = (state: RootState): Products[] =>
  state.products.products;
//get only one product
export const getProduct = (state: RootState): Products | undefined =>
  state.products.product;
//get applied filters
export const getProductFilters = (state: RootState): ProductsFilter =>
  state.products.productsFilter;
//get only filtered products
export const getFilteredProducts = (state: RootState): Products[] =>
  state.products.filteredProducts;
//get featured products
export const getFeaturedProduct = (state: RootState): Products[] | undefined =>
  state.products.featuredProducts;
