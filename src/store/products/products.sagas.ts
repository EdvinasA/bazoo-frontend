import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import { AxiosResponse } from 'axios';
import { apiEndpoint } from '../../constants/api';
import axiosInstance from '../../api/axiosInstance';
import {
  getAllProducts,
  getAllProductsFailure,
  getAllProductsSuccess,
  getSingleProduct,
  getSingleProductSuccess,
  getSingleProductFailure,
  getFeaturedProducts,
  getFeaturedProductsSuccess,
  getFeaturedProductsFailure,
  filterProducts,
  filterProductsSuccess,
  filterProductsFailure,
  searchProducts,
  searchProductsSuccess,
  searchProductsFailure,
} from './products.actions';
import { Products, ProductsFilter } from './types';
import { PayloadAction } from '@reduxjs/toolkit';
import { getProductFilters } from '.';

//---- fetch all products ----//
function* fetchProducts() {
  try {
    const { data }: AxiosResponse<Products[]> = yield call(
      axiosInstance.get,
      apiEndpoint.getAllProducts()
    );
    yield put(getAllProductsSuccess(data));
  } catch (error) {
    if (error instanceof Error) yield put(getAllProductsFailure());
  }
}
//---- fetch single product by id ----//
function* fetchSingleProduct(action: PayloadAction<string>) {
  try {
    const { data }: AxiosResponse<Products> = yield call(
      axiosInstance.get,
      apiEndpoint.getSingleProduct(action.payload)
    );
    yield put(getSingleProductSuccess(data));
  } catch (error) {
    if (error instanceof Error) yield put(getSingleProductFailure());
  }
}
//---- fetch featured products ----//
function* fetchFeaturedProducts() {
  try {
    const { data }: AxiosResponse<Products[]> = yield call(
      axiosInstance.get,
      apiEndpoint.getFeaturedProducts()
    );
    yield put(getFeaturedProductsSuccess(data));
  } catch (error) {
    if (error instanceof Error) yield put(getFeaturedProductsFailure());
  }
}
//---- fetch filtered products ----//
function* fetchFilteredProducts() {
  const appliedFilters: ProductsFilter = yield select(getProductFilters);
  try {
    const { data }: AxiosResponse<Products[]> = yield call(
      axiosInstance.post,
      apiEndpoint.getFilteredProducts(),
      { filters: appliedFilters }
    );
    yield put(filterProductsSuccess(data));
  } catch (error) {
    if (error instanceof Error) yield put(filterProductsFailure());
  }
}
//---- fetch search products ----//
function* fetchSearchProducts(action: PayloadAction<string>) {
  try {
    const { data }: AxiosResponse<Products[]> = yield call(
      axiosInstance.post,
      apiEndpoint.searchProducts(),
      { searchQuery: action.payload }
    );
    yield put(searchProductsSuccess(data));
  } catch (error) {
    if (error instanceof Error) yield put(searchProductsFailure());
  }
}

function* productsSaga(): Generator {
  yield all([
    takeLatest(getAllProducts.type, fetchProducts),
    takeLatest(getSingleProduct.type, fetchSingleProduct),
    takeLatest(getFeaturedProducts.type, fetchFeaturedProducts),
    takeLatest(filterProducts.type, fetchFilteredProducts),
    takeLatest(searchProducts.type, fetchSearchProducts),
  ]);
}

export default productsSaga;
