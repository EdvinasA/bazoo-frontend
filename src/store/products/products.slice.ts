import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { Products, ProductsFilter, ProductsFiltersTypes } from './types';
import {
  getAllProducts,
  getAllProductsSuccess,
  getAllProductsFailure,
  getFeaturedProducts,
  getFeaturedProductsSuccess,
  getFeaturedProductsFailure,
  getSingleProduct,
  getSingleProductSuccess,
  getSingleProductFailure,
  filterProducts,
  filterProductsSuccess,
  filterProductsFailure,
  searchProducts,
  searchProductsSuccess,
  searchProductsFailure,
} from './products.actions';

export interface ProductsState {
  products: Products[];
  product?: Products;
  productsFilter: ProductsFilter;
  filteredProducts: Products[];
  featuredProducts?: Products[];
  isLoading: boolean;
}

const initialState: ProductsState = {
  products: [],
  productsFilter: { category: '', gender: [], size: [], price: [] },
  filteredProducts: [],
  isLoading: false,
};

export const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    // apply filters
    applyProductsFilter: (state, action: PayloadAction<ProductsFilter>) => {
      state.productsFilter = { ...action.payload };
    },
    // delete applied filter
    deleteAppliedFilter: (
      state,
      action: PayloadAction<{ type: ProductsFiltersTypes; value: string }>
    ) => {
      const { productsFilter } = state;
      const itemIndex = productsFilter[action.payload.type].indexOf(
        action.payload.value
      );
      if (action.payload.type === ProductsFiltersTypes.Category) {
        state.productsFilter.category = '';
      } else {
        productsFilter[action.payload.type].splice(itemIndex, 1);
      }
    },
  },
  extraReducers: (builder) => {
    //---- get all products ----//
    builder.addCase(getAllProducts, (state) => {
      state.isLoading = true;
    });
    builder.addCase(
      getAllProductsSuccess,
      (state, action: PayloadAction<Products[]>) => {
        state.products = action.payload;
        state.isLoading = false;
      }
    );
    builder.addCase(getAllProductsFailure, (state) => {
      state.isLoading = false;
    });
    //---- get single product ----//
    builder.addCase(getSingleProduct, (state) => {
      state.isLoading = true;
    });
    builder.addCase(
      getSingleProductSuccess,
      (state, action: PayloadAction<Products>) => {
        state.product = action.payload;
        state.isLoading = false;
      }
    );
    builder.addCase(getSingleProductFailure, (state) => {
      state.isLoading = false;
    });
    //---- get featured products ----//
    builder.addCase(getFeaturedProducts, (state) => {
      state.isLoading = true;
    });
    builder.addCase(
      getFeaturedProductsSuccess,
      (state, action: PayloadAction<Products[]>) => {
        state.featuredProducts = action.payload;
        state.isLoading = false;
      }
    );
    builder.addCase(getFeaturedProductsFailure, (state) => {
      state.isLoading = false;
    });
    //---- get filtered products ----//
    builder.addCase(filterProducts, (state) => {
      state.isLoading = true;
    });
    builder.addCase(
      filterProductsSuccess,
      (state, action: PayloadAction<Products[]>) => {
        state.filteredProducts = action.payload;
        state.isLoading = false;
      }
    );
    builder.addCase(filterProductsFailure, (state) => {
      state.isLoading = false;
    });
    //---- search products ----//
    builder.addCase(searchProducts, (state, action: PayloadAction<string>) => {
      state.productsFilter.category = action.payload;
      state.isLoading = true;
    });
    builder.addCase(
      searchProductsSuccess,
      (state, action: PayloadAction<Products[]>) => {
        state.filteredProducts = action.payload;
        state.isLoading = false;
      }
    );
    builder.addCase(searchProductsFailure, (state) => {
      state.isLoading = false;
    });
  },
});

export const { applyProductsFilter, deleteAppliedFilter } =
  productsSlice.actions;
export default productsSlice.reducer;
