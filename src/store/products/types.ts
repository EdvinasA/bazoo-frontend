import { Images } from '../../components/Carousel/types';

export interface Products {
  slug: string;
  images: Images[];
  title: string;
  price: string;
  oldPrice?: string;
  quantity: number;
  category: string;
  gender: string[];
  size: string[];
  colors: string[];
  rating: number;
}

export interface ProductsFilter {
  category: string;
  gender: string[];
  size: string[];
  price: string[];
}

export enum ProductsFiltersTypes {
  Category = 'category',
  Gender = 'gender',
  Size = 'size',
  Price = 'price',
}
