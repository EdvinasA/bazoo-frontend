import { createAction } from '@reduxjs/toolkit';
import { Products } from './types';

//---- fetch products ----//
export const getAllProducts = createAction('products/GET_ALL_PRODUCTS');
export const getAllProductsSuccess = createAction<Products[]>(
  'products/GET_ALL_PRODUCTS_SUCCESS'
);
export const getAllProductsFailure = createAction(
  'products/GET_ALL_PRODUCTS_FAILURE'
);
//---- fetch single product ----//
export const getSingleProduct = createAction<string>(
  'products/GET_SINGLE_PRODUCT'
);
export const getSingleProductSuccess = createAction<Products>(
  'products/GET_SINGLE_PRODUCT_SUCCESS'
);
export const getSingleProductFailure = createAction(
  'products/GET_SINGLE_PRODUCT_FAILURE'
);
//---- fetch featured products ----//
export const getFeaturedProducts = createAction(
  'products/GET_FEATURED_PRODUCTS'
);
export const getFeaturedProductsSuccess = createAction<Products[]>(
  'products/GET_FEATURED_PRODUCTS_SUCCESS'
);
export const getFeaturedProductsFailure = createAction(
  'products/GET_FEATURED_PRODUCTS_FAILURE'
);
//---- fetch filtered products ----//
export const filterProducts = createAction('products/GET_FILTERED_PRODUCTS');
export const filterProductsSuccess = createAction<Products[]>(
  'products/GET_FILTERED_PRODUCTS_SUCCESS'
);
export const filterProductsFailure = createAction(
  'products/GET_FILTERED_PRODUCTS_FAILURE'
);
//---- fetch search products ----//
export const searchProducts = createAction<string>(
  'products/GET_SEARCH_PRODUCTS'
);
export const searchProductsSuccess = createAction<Products[]>(
  'products/GET_SEARCH_PRODUCTS_SUCCESS'
);
export const searchProductsFailure = createAction(
  'products/GET_SEARCH_PRODUCTS_FAILURE'
);
