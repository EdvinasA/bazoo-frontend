import {
  FlexWrapper,
  SpacerWrapper,
  TextWrapper,
} from '../../../../styles/wrappers';
import Modal from '../../../../components/Modal/Modal';
import { useProductModal } from './useProductModal';
import Image from '../../../../components/Image/Image';
import { ProductDetailContainer, ProductImageContainer } from './styles';
import { buttons } from './data';
import Button from '../../../../components/Button/Button';

const ProductModal = (): JSX.Element => {
  const { isOpen, handleModalClose, handleButtonClick, productDetails } =
    useProductModal();
  return (
    <Modal isOpen={isOpen} close={handleModalClose}>
      <SpacerWrapper padding='25px'>
        <TextWrapper textAlign='center' fontSize='font20' fontWeight='600'>
          Your product has been added to the cart
        </TextWrapper>
        <ProductDetailContainer>
          <ProductImageContainer>
            <Image
              src={productDetails?.image ?? ''}
              alt={'product-image'}
              withEffect={false}
              width='100%'
              height='100%'
              style={{ objectFit: 'cover' }}
            />
          </ProductImageContainer>
          <div>
            <TextWrapper fontSize='font23'>{productDetails?.name}</TextWrapper>
            <SpacerWrapper marginBottom='25px' />
            <FlexWrapper gap='10px'>
              <TextWrapper fontWeight='600'>Quantity:</TextWrapper>
              <TextWrapper>{productDetails?.quantity}</TextWrapper>
            </FlexWrapper>
          </div>
        </ProductDetailContainer>
        <FlexWrapper justifyContent='space-evenly' marginTop='35px' gap='20px'>
          {buttons.map((item, index) => (
            <Button
              key={index}
              title={item.title}
              backgroundColor={index === 0 ? 'white' : 'gunmetal'}
              isBorder={index === 0}
              textColor={index === 0 ? 'gunmetal' : 'white'}
              width={index === 0 ? '250px' : '150px'}
              height='45px'
              onClick={() => handleButtonClick(item.action)}
            />
          ))}
        </FlexWrapper>
      </SpacerWrapper>
    </Modal>
  );
};

export default ProductModal;
