import { ButtonsActions } from './types';

export const buttons = [
  {
    title: 'Keep shopping',
    action: ButtonsActions.Shop,
  },
  { title: 'Go to cart', action: ButtonsActions.Cart },
];
