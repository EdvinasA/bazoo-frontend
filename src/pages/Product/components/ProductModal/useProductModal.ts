import { useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import {
  getCartProducts,
  isCartMessageModalOpen,
} from '../../../../store/cart';
import { setIsCartMessageModalOpen } from '../../../../store/cart/cart.slice';
import { ButtonsActions } from './types';
import { routePath } from '../../../../constants/routes';
import { useEffect, useState } from 'react';

export const useProductModal = () => {
  const isOpen = useAppSelector(isCartMessageModalOpen);
  const cartProducts = useAppSelector(getCartProducts);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [productDetails, setProductDetails] = useState<{
    image: string;
    name: string;
    quantity: number;
  }>();

  const handleModalClose = () => {
    dispatch(setIsCartMessageModalOpen(false));
  };

  const handleButtonClick = (action: ButtonsActions) => {
    switch (action) {
      case ButtonsActions.Shop:
        handleModalClose();
        navigate(routePath.catalog);
        break;
      case ButtonsActions.Cart:
        handleModalClose();
        navigate(routePath.cart);
        break;
      default:
        console.log('default');
    }
  };

  useEffect(() => {
    const product = cartProducts.find(
      (_, index, { length }) => index === length - 1
    );
    setProductDetails({
      image: product?.image ?? '',
      name: product?.title ?? '',
      quantity: product?.quantity ?? 0,
    });
  }, [cartProducts]);
  return { isOpen, handleModalClose, handleButtonClick, productDetails };
};
