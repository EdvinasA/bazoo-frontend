import styled from 'styled-components';
import { DeviceSizes } from '../../../../constants/deviceSizes';

export const ProductDetailContainer = styled.div`
  display: flex;
  margin-top: 35px;
  gap: 35px;
  @media only screen and (max-width: ${DeviceSizes.mobile}px) {
    flex-wrap: wrap-reverse;
    justify-content: center;
  }
`;

export const ProductImageContainer = styled.div`
  width: 100%;
  max-width: 220px;
  height: 250px;
  border-radius: 3px;
  overflow: hidden;
`;
