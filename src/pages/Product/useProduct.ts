import { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { getProduct } from '../../store/products';
import {
  addToCart,
  updateTotalPriceAndQuantity,
} from '../../store/cart/cart.slice';
import { useParams } from 'react-router-dom';
import { InputNumberActions } from '../../components/InputNumber/types';
import { ProductDetails } from './types';
import { toast } from 'react-toastify';
import { getSingleProduct } from '../../store/products/products.actions';

const initialProductDetails: ProductDetails = {
  size: '',
  color: '',
  quantity: 1,
};

export const useProduct = () => {
  const product = useAppSelector(getProduct);
  const dispatch = useAppDispatch();
  const { slug } = useParams();
  const [orderDetails, setOrderDetails] = useState<ProductDetails>(
    initialProductDetails
  );
  const [colorIndex, setColorIndex] = useState<number>(0);
  const minPurchasebleValue = 1;
  const minProductQuantity = 3;

  const onMouseWheelChangeHandler = () => {
    console.log('mouse wheel');
  };

  // handle quantity change
  const handleQuantityChange = (action: string) => {
    switch (action) {
      case InputNumberActions.Increase:
        setOrderDetails((prevValue) => {
          const newValue = { ...prevValue, quantity: prevValue.quantity + 1 };
          if (product && newValue.quantity <= product.quantity) {
            return { ...prevValue, quantity: newValue.quantity };
          }
          return { ...prevValue, quantity: prevValue.quantity };
        });
        break;
      case InputNumberActions.Decrease:
        setOrderDetails((prevValue) => {
          const newValue = { ...prevValue, quantity: prevValue.quantity - 1 };
          if (newValue.quantity >= minPurchasebleValue) {
            return { ...prevValue, quantity: newValue.quantity };
          }
          return { ...prevValue, quantity: prevValue.quantity }; // If the newValue is out of range, return the previous values unchanged.
        });
        break;
    }
  };
  //product size handler
  const handleProductSizeSelect = (size: string) => {
    setOrderDetails({ ...orderDetails, size: size });
  };
  // product color handler
  const handleProductColorSelect = (color: string, colorIndex: number) => {
    setOrderDetails({ ...orderDetails, color: color });
    setColorIndex(colorIndex);
  };
  //add to cart handler
  const handleAddToCartButtonClick = () => {
    if (product && orderDetails.size !== '' && orderDetails.color !== '') {
      dispatch(
        addToCart({
          slug: product.slug,
          title: product.title,
          image: product.images[colorIndex].src,
          price: product.price,
          productCount: product.quantity,
          ...orderDetails,
        })
      );
      dispatch(updateTotalPriceAndQuantity());
    } else {
      toast.error('Select product details to proceed.');
    }
  };

  useEffect(() => {
    dispatch(getSingleProduct(slug || ''));
  }, [slug]);

  useEffect(() => {
    if (slug) {
      setOrderDetails(initialProductDetails);
      window.scrollTo({
        top: 0,
        left: 0,
        behavior: 'smooth',
      });
    }
  }, [slug]);

  return {
    product,
    handleQuantityChange,
    orderDetails,
    handleProductSizeSelect,
    handleProductColorSelect,
    handleAddToCartButtonClick,
    onMouseWheelChangeHandler,
    minProductQuantity,
  };
};
