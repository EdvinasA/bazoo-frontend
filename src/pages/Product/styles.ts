import styled from 'styled-components';

export const ProductContainer = styled.div`
  display: grid;
  grid-template-columns: 1.1fr 0.9fr;
  gap: 40px;
  @media only screen and (max-width: 650px) {
    grid-template-columns: 1fr;
  }
`;

export const Border = styled.div`
  width: 100%;
  border: ${({ theme }) => `1px solid ${theme.colors.darkerAntiFlashWhite}`};
  margin: 25px 0px;
`;

export const RatingValue = styled.span`
  margin-left: 7px;
`;
