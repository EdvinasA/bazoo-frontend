import { Icon, IconNames } from '../../assets/icons';
import Button from '../../components/Button/Button';
import Heading from '../../components/Heading/Heading';
import InputNumber from '../../components/InputNumber/InputNumber';
import { FlexWrapper, SpacerWrapper, TextWrapper } from '../../styles/wrappers';
import ProductModal from './components/ProductModal/ProductModal';
import { ProductContainer, Border, RatingValue } from './styles';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useProduct } from './useProduct';
import Carousel from '../../components/Carousel/Carousel';
import FeaturedProducts from '../../components/FeaturedProducts/FeaturedProducts';
import { Rating } from 'react-simple-star-rating';
import ButtonCheckbox from '../../components/ButtonCheckbox/ButtonCheckbox';

const Product = (): JSX.Element => {
  const {
    product,
    handleQuantityChange,
    orderDetails,
    handleProductSizeSelect,
    handleProductColorSelect,
    handleAddToCartButtonClick,
    onMouseWheelChangeHandler,
    minProductQuantity,
  } = useProduct();
  // render product quantity
  const renderProductQuantity = () => {
    if (product && product.quantity <= minProductQuantity) {
      return (
        <TextWrapper>
          Only {product?.quantity} left - make it yours!
        </TextWrapper>
      );
    }
    if (product && product.quantity >= minProductQuantity) {
      return <TextWrapper>Quantity: {product.quantity}</TextWrapper>;
    }
  };
  return (
    <>
      <ProductContainer>
        <div>
          <Carousel
            images={product ? product.images : []}
            isDots={false}
            bottomImages
            backForwardButtonsProps={{
              justifyContent: 'space-between',
              top: '50%',
              width: '45px',
              height: '30px',
            }}
          />
        </div>
        <div>
          <Heading title={product?.title ?? ''} margin='0px 0px 15px 0px' />
          <Rating
            readonly
            allowFraction
            initialValue={product?.rating}
            size={27}
          />
          <RatingValue>({product?.rating})</RatingValue>
          <FlexWrapper marginTop='20px'>
            <Icon iconName={IconNames.DollarIcon} width={25} fill='gunmetal' />
            <SpacerWrapper marginLeft='-4px'>
              <TextWrapper fontSize='font30' fontWeight='600' color='gunmetal'>
                {product?.price}
              </TextWrapper>
            </SpacerWrapper>
          </FlexWrapper>
          <Border />
          <FlexWrapper gap='35px' flexWrap='wrap'>
            <SpacerWrapper width='100%'>
              <TextWrapper color='gunmetal'>Available Size</TextWrapper>
              <FlexWrapper gap='10px' marginTop='15px' flexWrap='wrap'>
                {product?.size.map((size, index) => (
                  <Button
                    key={index}
                    title={size.toUpperCase()}
                    width='50px'
                    height='50px'
                    onClick={() => handleProductSizeSelect(size)}
                    isActive={orderDetails.size === size}
                    isBorder
                    isHover
                  />
                ))}
              </FlexWrapper>
            </SpacerWrapper>
            <SpacerWrapper>
              <TextWrapper color='gunmetal'>Available Color</TextWrapper>
              <FlexWrapper
                gap='25px'
                marginTop='15px'
                height='50px'
                alignItems='center'
                marginLeft='5px'
              >
                {product?.colors.map((color, index) => (
                  <ButtonCheckbox
                    key={index}
                    title={color}
                    titleHidden
                    width='18px'
                    height='18px'
                    backgroundColor={color}
                    borderRadius={'50%'}
                    isChecked={orderDetails.color === color}
                    isBorder
                    onClick={() => handleProductColorSelect(color, index)}
                  />
                ))}
              </FlexWrapper>
            </SpacerWrapper>
          </FlexWrapper>
          <Border />
          {renderProductQuantity()}
          <SpacerWrapper marginBottom='25px' />
          <FlexWrapper justifyContent='space-between' width='275px'>
            <InputNumber
              value={orderDetails.quantity}
              onChange={onMouseWheelChangeHandler}
              handleNumberChange={(item) => handleQuantityChange(item)}
            />
            <Button
              onClick={handleAddToCartButtonClick}
              title='Add to cart'
              width='130px'
              height='45px'
              backgroundColor='gunmetal'
              textColor='white'
            />
          </FlexWrapper>
        </div>
      </ProductContainer>
      <FeaturedProducts title='Related products' />
      <ProductModal />
      <ToastContainer position='top-center' />
    </>
  );
};

export default Product;
