export interface ProductDetails {
  size: string;
  color: string;
  quantity: number;
}
