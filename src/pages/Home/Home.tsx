import MainCarousel from './components/MainCarousel/MainCarousel';
import Brands from './components/Brands/Brands';
import UserGuarantee from './components/UserGuarantee/UserGuarantee';
import CurratedPicks from './components/CurratedPicks/CurratedPicks';
import FeaturedProducts from '../../components/FeaturedProducts/FeaturedProducts';
import Banner from '../../components/Banner/Banner';
import Subscribe from '../../components/Subscribe/Subscribe';

const Home = (): JSX.Element => {
  return (
    <>
      <MainCarousel />
      <Brands />
      <UserGuarantee />
      <CurratedPicks />
      <FeaturedProducts title='Featured products' />
      <Banner />
      <Subscribe />
    </>
  );
};

export default Home;
