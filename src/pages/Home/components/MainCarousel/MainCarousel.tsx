import Carousel from '../../../../components/Carousel/Carousel';
import { IconNames } from '../../../../assets/icons';
import { useNavigate } from 'react-router-dom';
import { routePath } from '../../../../constants/routes';
import { images } from './data';

const MainCarousel = (): JSX.Element => {
  const navigate = useNavigate();
  return (
    <Carousel
      images={images}
      text='Level up your style with our summer collections'
      isDots
      isButton
      isOverlay
      bottomImages={false}
      buttonTitle='Shop now'
      titleIcon={IconNames.ArrowRight}
      onClick={() => navigate(routePath.catalog)}
      height='650px'
      backForwardButtonsProps={{
        justifyContent: 'flex-end',
        top: '50px',
        width: '50px',
        height: '45px',
      }}
    />
  );
};

export default MainCarousel;
