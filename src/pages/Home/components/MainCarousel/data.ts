import { Images } from '../../../../components/Carousel/types';

export const images: Images[] = [
  {
    src: 'https://images.unsplash.com/photo-1557991666-3dc7eae97614?q=80&w=2069&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    alt: 'clothes-1',
  },
  {
    src: 'https://images.unsplash.com/photo-1465877783223-4eba513e27c6?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    alt: 'clothes-2',
  },
  {
    src: 'https://images.unsplash.com/photo-1556540295-ddfafd043626?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    alt: 'clothes-3',
  },
];
