import { BrandIconNames } from '../../../../assets/icons/brands';

interface BrandsData {
  icon: BrandIconNames;
}

export const brandsData: BrandsData[] = [
  { icon: BrandIconNames.Chanel },
  { icon: BrandIconNames.Nike },
  { icon: BrandIconNames.GuesJeans },
  { icon: BrandIconNames.Gucci },
  { icon: BrandIconNames.VansLogo },
  { icon: BrandIconNames.Adidas },
  { icon: BrandIconNames.Levis },
  { icon: BrandIconNames.Versace },
];
