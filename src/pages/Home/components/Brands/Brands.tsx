import { BrandsIcons } from '../../../../assets/icons/brands';
import Heading from '../../../../components/Heading/Heading';
import { FlexWrapper } from '../../../../styles/wrappers';
import { brandsData } from './data';

const Brands = (): JSX.Element => {
  return (
    <section>
      <Heading title='Brands' margin='40px 0px 50px 0px' />
      <FlexWrapper justifyContent='space-between' gap='12px'>
        {brandsData.map((item, index) => (
          <BrandsIcons key={index} iconName={item.icon} width={70} />
        ))}
      </FlexWrapper>
    </section>
  );
};

export default Brands;
