import { GuaranteeIconNames } from '../../../../assets/icons/guarantee';
import { UserGuaranteeData } from './types';

export const userGuaranteeData: UserGuaranteeData[] = [
  {
    icon: GuaranteeIconNames.MoneySend,
    title: 'Original Products',
    text: 'We provide money back guarantee if the product are not original',
  },
  {
    icon: GuaranteeIconNames.Mask,
    title: 'Satisfaction Guarantee',
    text: `Exchange the product you've purchased if it doesn't fit on you`,
  },
  {
    icon: GuaranteeIconNames.Delivery,
    title: 'New Arrival Everyday',
    text: 'We update our collections almost everyday',
  },
  {
    icon: GuaranteeIconNames.Truck,
    title: 'Fast & Free Shipping',
    text: 'We offer fast and free shipping for our loyal customers',
  },
];
