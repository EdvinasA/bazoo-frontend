import { GuaranteeIcons } from '../../../../assets/icons/guarantee';
import {
  FlexWrapper,
  SpacerWrapper,
  TextWrapper,
} from '../../../../styles/wrappers';
import { userGuaranteeData } from './data';
import {
  ContentContainer,
  Title,
  ParagraphContainer,
  IconContainer,
  Devider,
} from './styles';

const UserGuarantee = (): JSX.Element => {
  return (
    <section>
      <FlexWrapper
        marginTop='105px'
        justifyContent='space-between'
        alignItems='center'
        marginBottom='40px'
        flexWrap='wrap'
        gap='30px'
      >
        <Title>We provide best customer experiences</Title>
        <ParagraphContainer>
          <Devider />
          <TextWrapper color='cadetGray'>
            We ensure our customers have the best shopping experience
          </TextWrapper>
        </ParagraphContainer>
      </FlexWrapper>
      <FlexWrapper flexWrap='wrap' justifyContent='space-between'>
        {userGuaranteeData.map((item, index) => (
          <ContentContainer key={index}>
            <IconContainer>
              <GuaranteeIcons iconName={item.icon} width={30} height={30} />
            </IconContainer>
            <SpacerWrapper margin='16px 0px 10px 0px'>
              <TextWrapper fontSize='font23' fontWeight='600' color='gunmetal'>
                {item.title}
              </TextWrapper>
            </SpacerWrapper>
            <TextWrapper lineHeight='lh26' color='cadetGray'>
              {item.text}
            </TextWrapper>
          </ContentContainer>
        ))}
      </FlexWrapper>
    </section>
  );
};

export default UserGuarantee;
