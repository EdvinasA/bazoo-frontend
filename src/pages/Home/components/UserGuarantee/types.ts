import { GuaranteeIconNames } from '../../../../assets/icons/guarantee';

export interface UserGuaranteeData {
  icon: GuaranteeIconNames;
  title: string;
  text: string;
}
