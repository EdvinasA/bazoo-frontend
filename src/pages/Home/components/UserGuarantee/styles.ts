import styled from 'styled-components';

export const ContentContainer = styled.div`
  width: 23%;
  margin: 5px 0px 35px 0px;
  @media only screen and (max-width: 895px) {
    width: 48%;
  }
`;

export const Title = styled.h2`
  width: 100%;
  max-width: 370px;
  font-size: ${({ theme }) => theme.fontSize.font30};
  color: ${({ theme }) => theme.colors.gunmetal};
`;

export const ParagraphContainer = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  max-width: 530px;
`;

export const IconContainer = styled.div`
  width: 65px;
  height: 65px;
  padding: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #f2f4f7;
  border-radius: 10px;
  margin-bottom: 10px;
`;

export const Devider = styled.div`
  height: 100px;
  border-left: ${({ theme }) => `4px solid ${theme.colors.gunmetal}`};
  margin-right: 45px;
`;
