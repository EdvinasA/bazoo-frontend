import styled from 'styled-components';

export const ContentContainer = styled.div`
  position: relative;
  width: 23%;
  height: 280px;
  margin: 10px 0px 10px 0px;
  @media only screen and (max-width: 895px) {
    width: 48%;
    height: 30vh;
  }
`;

export const ImageContainer = styled.div`
  border-radius: 10px;
  overflow: hidden;
  height: 100%;
`;
