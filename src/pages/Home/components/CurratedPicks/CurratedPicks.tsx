import Heading from '../../../../components/Heading/Heading';
import Image from '../../../../components/Image/Image';
import { FlexWrapper } from '../../../../styles/wrappers';
import { curratedPicks } from './data';
import { ContentContainer, ImageContainer } from './styles';

const CurratedPicks = (): JSX.Element => {
  return (
    <section>
      <Heading title='Currated picks' margin='45px 0px 30px 0px' />
      <FlexWrapper justifyContent='space-between' flexWrap='wrap'>
        {curratedPicks.map((item, index) => (
          <ContentContainer key={index}>
            <ImageContainer>
              <Image
                src={item.image}
                withEffect
                alt='kwa-kwa'
                width='100%'
                height='100%'
                style={{ objectFit: 'cover' }}
              />
            </ImageContainer>
          </ContentContainer>
        ))}
      </FlexWrapper>
    </section>
  );
};

export default CurratedPicks;
