import { useState } from 'react';
import Banner from '../../components/Banner/Banner';
import Subscribe from '../../components/Subscribe/Subscribe';
import { FlexWrapper } from '../../styles/wrappers';
import MobileFilterPanel from './components/MobileFilterPanel/MobileFilterPanel';
import Products from './components/Products/Products';
import SidePanel from './components/SidePanel/SidePanel';
import MobileFilterButton from './components/MobileFilterPanel/components/MobileFilterButton';

const Catalog = (): JSX.Element => {
  const [isMobileFilterPanelOpen, setMobileFilterPanelOpen] = useState(false);
  return (
    <>
      <MobileFilterButton onClick={() => setMobileFilterPanelOpen(true)} />
      <FlexWrapper width='100%' gap='25px'>
        <SidePanel />
        <Products />
      </FlexWrapper>
      <Banner />
      <Subscribe />
      <MobileFilterPanel
        isOpen={isMobileFilterPanelOpen}
        onClose={() => setMobileFilterPanelOpen(false)}
      />
    </>
  );
};

export default Catalog;
