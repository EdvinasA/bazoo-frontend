import styled from 'styled-components';
import { DeviceSizes } from '../../../../constants/deviceSizes';

interface MobileFilterPanelOverlayProps {
  isOpen: boolean;
}

export const MobileFilterPanelOverlay = styled.div<MobileFilterPanelOverlayProps>`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  display: ${({ isOpen }) => (isOpen ? 'block' : 'none')};
`;

interface MobileFilterPanelContainerProps {
  isOpen: boolean;
}

export const MobileFilterPanelContainer = styled.div<MobileFilterPanelContainerProps>`
  width: 100%;
  height: ${({ isOpen }) => (isOpen ? '665px' : '0px')};
  background-color: ${({ theme }) => theme.colors.white};
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  transition: height 300ms ease-in-out;
  /* width */
  ::-webkit-scrollbar {
    width: 7px;
  }
  /* Track */
  ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px grey;
    border-radius: 10px;
  }
  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: ${({ theme }) => theme.colors.gunmetal};
    border-radius: 10px;
  }
  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: ${({ theme }) => theme.colors.gunmetal};
  }
  @media only screen and (max-width: ${DeviceSizes.mobile}px) {
    overflow-y: auto;
  }
  @media only screen and (max-height: 720px) {
    height: ${({ isOpen }) => (isOpen ? '600px' : '0px')};
    overflow-y: auto;
  }
`;

export const MobileFilterPanelBorder = styled.div`
  border: ${({ theme }) => `1px solid ${theme.colors.darkerAntiFlashWhite}`};
  margin-top: 15px;
`;
