import { useCallback, useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import { getProductFilters } from '../../../../store/products';
import {
  ProductsFilter,
  ProductsFiltersTypes,
} from '../../../../store/products/types';
import { MobileFilterPanelButtonsActions } from './types';
import { filterProducts } from '../../../../store/products/products.actions';
import { applyProductsFilter } from '../../../../store/products/products.slice';

export const useMobileFilterPanel = (onClose: () => void) => {
  const dispatch = useAppDispatch();
  const appliedFilters = useAppSelector(getProductFilters);
  const [totalFiltersCount, setTotalFiltersCount] = useState<number>(0);
  const [selectedFilters, setSelectedFilters] = useState<ProductsFilter>({
    category: '',
    gender: [],
    size: [],
    price: [],
  });
  // ---- Handlers ---- //
  const handleRootPorductsFilter = (category: string) => {
    setSelectedFilters({
      ...selectedFilters,
      category: category.toLowerCase(),
    });
  };

  const handleCategoryChecked = useCallback(
    (checked: boolean, category: string, type: ProductsFiltersTypes) => {
      if (checked) {
        setSelectedFilters({
          ...selectedFilters,
          [type]: [...selectedFilters[type], category.toLowerCase()],
        });
      }
      if (!checked) {
        if (type !== ProductsFiltersTypes.Category) {
          const newArray = selectedFilters[type].filter(
            (item) => item !== category.toLowerCase()
          );
          setSelectedFilters({ ...selectedFilters, [type]: newArray });
        }
      }
    },
    [selectedFilters]
  );

  const handleMobileFilterPanelButtons = useCallback(
    (action: MobileFilterPanelButtonsActions) => {
      switch (action) {
        case MobileFilterPanelButtonsActions.Apply:
          dispatch(
            applyProductsFilter({
              ...selectedFilters,
            })
          );
          dispatch(filterProducts());
          onClose();
          break;
        case MobileFilterPanelButtonsActions.Clear:
          setSelectedFilters({ category: '', gender: [], size: [], price: [] });
      }
    },
    [selectedFilters]
  );
  const isCheckboxChecked = useCallback(
    (category: string) => {
      for (let key in appliedFilters)
        if (
          selectedFilters.category === category.toLowerCase() ||
          (selectedFilters[key as keyof ProductsFilter] !==
            selectedFilters.category &&
            selectedFilters[key as keyof ProductsFilter].includes(
              category.toLowerCase()
            ))
        )
          return true;
      return false;
    },
    [selectedFilters]
  );

  const countTotalSelectedFilters = (selectedFilters: ProductsFilter) => {
    let totalSelectedFilters: number;
    totalSelectedFilters = Object.values(selectedFilters).reduce(
      (a, v) => a + (Array.isArray(v) ? v.length : 0),
      0
    );
    if (selectedFilters.category !== '') totalSelectedFilters += 1;
    setTotalFiltersCount(totalSelectedFilters);
  };

  // ---- useEffects ---- //
  useEffect(() => {
    setSelectedFilters({ ...appliedFilters });
  }, [appliedFilters]);

  useEffect(() => {
    countTotalSelectedFilters(selectedFilters);
  }, [selectedFilters]);

  return {
    handleRootPorductsFilter,
    handleCategoryChecked,
    handleMobileFilterPanelButtons,
    isCheckboxChecked,
    totalFiltersCount,
  };
};
