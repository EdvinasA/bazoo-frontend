export enum MobileFilterPanelButtonsActions {
  Clear = 'clear',
  Apply = 'apply',
}

export interface MobileFilterPanelButtonsData {
  title: string;
  action: MobileFilterPanelButtonsActions;
}
