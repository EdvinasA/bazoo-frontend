import { FC, Fragment } from 'react';
import {
  MobileFilterPanelOverlay,
  MobileFilterPanelContainer,
  MobileFilterPanelBorder,
} from './styles';
import {
  FlexWrapper,
  PositionWrapper,
  SpacerWrapper,
  TextWrapper,
} from '../../../../styles/wrappers';
import Button from '../../../../components/Button/Button';
import { IconNames } from '../../../../assets/icons';
import { categories } from '../SidePanel/categories';
import Checkbox from '../../../../components/Checkbox/Checkbox';
import { mobileFilterPanelButtons } from './data';
import SubcategoryTitle from '../SidePanel/components/SubcategoryTitle/SubcategoryTitle';
import { useMobileFilterPanel } from './useMobileFilterPanel';
import { ProductsFiltersTypes } from '../../../../store/products/types';
import ButtonCheckbox from '../../../../components/ButtonCheckbox/ButtonCheckbox';

interface MobileFilterPanelProps {
  isOpen: boolean;
  onClose: () => void;
}

const MobileFilterPanel: FC<MobileFilterPanelProps> = ({
  isOpen,
  onClose,
}): JSX.Element => {
  const {
    handleRootPorductsFilter,
    handleCategoryChecked,
    handleMobileFilterPanelButtons,
    isCheckboxChecked,
    totalFiltersCount,
  } = useMobileFilterPanel(onClose);
  return (
    <>
      <MobileFilterPanelOverlay isOpen={isOpen} />
      <MobileFilterPanelContainer isOpen={isOpen}>
        <SpacerWrapper margin='25px'>
          <PositionWrapper position='absolute' top='15px' right='15px'>
            <Button
              iconSrc={IconNames.CloseCircleIcon}
              iconWidth={30}
              width='30px'
              onClick={onClose}
            />
          </PositionWrapper>
          <SpacerWrapper margin='20px 0px'>
            <TextWrapper fontWeight='600' fontSize='font18' color='gunmetal'>
              Filter
            </TextWrapper>
          </SpacerWrapper>
          {categories.map((item, index) => (
            <Fragment key={index}>
              {item.title === 'Categories' ? (
                <>
                  <TextWrapper color='charcoal' fontSize='base'>
                    {item.title}
                  </TextWrapper>
                  <FlexWrapper flexWrap='wrap' gap='13px' marginTop='15px'>
                    {item.categories.map((category, index) => (
                      <ButtonCheckbox
                        key={index}
                        title={category}
                        width={'85px'}
                        height={'35px'}
                        backgroundColor={'white'}
                        isBorder
                        isMobileCategoryBtn
                        borderRadius={'22px'}
                        isChecked={isCheckboxChecked(category)}
                        onClick={() => handleRootPorductsFilter(category)}
                        titleHidden={false}
                      />
                    ))}
                  </FlexWrapper>
                </>
              ) : (
                <>
                  {index !== 0 && <MobileFilterPanelBorder />}
                  <SpacerWrapper margin='15px 0px'>
                    <TextWrapper color='charcoal' fontSize='base'>
                      {item.title}
                    </TextWrapper>
                  </SpacerWrapper>
                  <FlexWrapper flexWrap='wrap' gap='15px'>
                    {item.categories.map((category, index) => (
                      <Fragment key={index}>
                        {item.title === 'Size' && index === 0 && (
                          <SubcategoryTitle title='Clothes' />
                        )}
                        {item.title === 'Size' && index === 6 && (
                          <SubcategoryTitle title='Shoes' />
                        )}
                        <Checkbox
                          title={category}
                          value={category.toLowerCase()}
                          onChange={(event) =>
                            handleCategoryChecked(
                              event.target.checked,
                              category,
                              item.title.toLowerCase() as ProductsFiltersTypes
                            )
                          }
                          isChecked={isCheckboxChecked(category)}
                        />
                      </Fragment>
                    ))}
                  </FlexWrapper>
                </>
              )}
            </Fragment>
          ))}
          <FlexWrapper gap='45px' marginTop='40px'>
            {mobileFilterPanelButtons.map((button, index) => (
              <Button
                key={index}
                title={
                  button.title === 'Clear' && totalFiltersCount > 0
                    ? button.title + ` (${totalFiltersCount.toString()})`
                    : button.title
                }
                onClick={() => handleMobileFilterPanelButtons(button.action)}
                height='45px'
                backgroundColor={index === 1 ? 'gunmetal' : 'white'}
                textColor={index === 0 ? 'gunmetal' : 'white'}
                isBorder
              />
            ))}
          </FlexWrapper>
        </SpacerWrapper>
      </MobileFilterPanelContainer>
    </>
  );
};

export default MobileFilterPanel;
