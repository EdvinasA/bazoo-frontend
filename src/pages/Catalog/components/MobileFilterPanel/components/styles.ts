import styled from 'styled-components';
import { DeviceSizes } from '../../../../../constants/deviceSizes';

export const MobileFilterButtonContainer = styled.div`
  display: none;
  @media only screen and (max-width: ${DeviceSizes.laptop}px) {
    display: flex;
    justify-content: flex-end;
    margin-bottom: 25px;
  }
`;
