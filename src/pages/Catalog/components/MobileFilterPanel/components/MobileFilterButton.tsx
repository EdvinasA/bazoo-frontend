import { FC } from 'react';
import { IconNames } from '../../../../../assets/icons';
import Button from '../../../../../components/Button/Button';
import { MobileFilterButtonContainer } from './styles';

interface MobileFilterButtonProps {
  onClick: () => void;
}

const MobileFilterButton: FC<MobileFilterButtonProps> = ({
  onClick,
}): JSX.Element => {
  return (
    <MobileFilterButtonContainer>
      <Button
        title='Filter'
        titleIconRight={IconNames.FilterOptionsIcon}
        width='95px'
        height='37px'
        isBorder
        onClick={onClick}
      />
    </MobileFilterButtonContainer>
  );
};

export default MobileFilterButton;
