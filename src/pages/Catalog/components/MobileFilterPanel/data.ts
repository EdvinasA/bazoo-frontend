import {
  MobileFilterPanelButtonsData,
  MobileFilterPanelButtonsActions,
} from './types';

export const mobileFilterPanelButtons: MobileFilterPanelButtonsData[] = [
  { title: 'Clear', action: MobileFilterPanelButtonsActions.Clear },
  { title: 'Apply', action: MobileFilterPanelButtonsActions.Apply },
];
