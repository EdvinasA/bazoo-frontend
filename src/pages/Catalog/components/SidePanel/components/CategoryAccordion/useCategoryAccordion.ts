import { useAppSelector } from '../../../../../../hooks/redux';
import { getProductFilters } from '../../../../../../store/products';
import { ProductsFilter } from '../../../../../../store/products/types';

export const useCategoryAccordion = () => {
  const appliedFilters = useAppSelector(getProductFilters);

  const isCategoryCheckboxChecked = (item: string) => {
    for (let key in appliedFilters) {
      if (
        appliedFilters[key as keyof ProductsFilter] !==
          appliedFilters.category &&
        appliedFilters[key as keyof ProductsFilter].includes(item.toLowerCase())
      )
        return true;
    }
    return false;
  };
  return { isCategoryCheckboxChecked };
};
