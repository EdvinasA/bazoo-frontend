import { FC, Fragment } from 'react';
import Accordion from '../../../../../../components/Accordion/Accordion';
import { SpacerWrapper } from '../../../../../../styles/wrappers';
import { BorderDevider } from '../../styles';
import Checkbox from '../../../../../../components/Checkbox/Checkbox';
import { useCategoryAccordion } from './useCategoryAccordion';
import SubcategoryTitle from '../SubcategoryTitle/SubcategoryTitle';

interface CategoryAccordionProps {
  onClick: () => void;
  title: string;
  isActive: boolean;
  isAccordionOpen: boolean;
  data: string[];
  handleChecked: (checked: boolean, category: string) => void;
}

const CategoryAccordion: FC<CategoryAccordionProps> = ({
  onClick,
  title,
  isActive,
  isAccordionOpen,
  data,
  handleChecked,
}) => {
  const { isCategoryCheckboxChecked } = useCategoryAccordion();
  return (
    <>
      <BorderDevider />
      <Accordion
        onClick={onClick}
        title={title}
        isActive={isActive}
        background
        isOpen={isAccordionOpen}
      >
        <SpacerWrapper margin='15px'>
          {data.map((item, index) => (
            <Fragment key={index}>
              {title === 'Size' && index === 0 && (
                <SubcategoryTitle title='Clothes' margin='15px 0px 15px 0px' />
              )}
              {title === 'Size' && index === 6 && (
                <SubcategoryTitle title='Shoes' margin='15px 0px 15px 0px' />
              )}
              <Checkbox
                title={item}
                value={item.toLowerCase()}
                onChange={(event) => handleChecked(event.target.checked, item)}
                isChecked={isCategoryCheckboxChecked(item)}
              />
            </Fragment>
          ))}
        </SpacerWrapper>
      </Accordion>
    </>
  );
};

export default CategoryAccordion;
