import { FC } from 'react';
import { SpacerWrapper, TextWrapper } from '../../../../../../styles/wrappers';

interface SubcategoryTitleProps {
  title: string;
  margin?: string;
}

const SubcategoryTitle: FC<SubcategoryTitleProps> = ({
  title,
  margin,
}): JSX.Element => {
  return (
    <SpacerWrapper margin={margin} width='100%'>
      <TextWrapper color='charcoal'>{title}:</TextWrapper>
    </SpacerWrapper>
  );
};

export default SubcategoryTitle;
