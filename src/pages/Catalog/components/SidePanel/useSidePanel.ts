import { useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import {
  deleteAppliedFilter,
  applyProductsFilter,
} from '../../../../store/products/products.slice';
import { getProductFilters } from '../../../../store/products';
import { ProductsFiltersTypes } from '../../../../store/products/types';
import { filterProducts } from '../../../../store/products/products.actions';

export const useSidePanel = () => {
  const dispatch = useAppDispatch();
  const [section, setSection] = useState<number | undefined>(undefined);
  const appliedFilters = useAppSelector(getProductFilters);

  //---- handlers ----//
  const handleSectionClick = (index: number) => {
    setSection(index);
  };
  const handleRootPorductsFilter = (category: string) => {
    dispatch(
      applyProductsFilter({
        ...appliedFilters,
        category: category.toLowerCase(),
      })
    );
    dispatch(filterProducts());
  };
  const handleCategoryChecked = (
    checked: boolean,
    category: string,
    type: ProductsFiltersTypes
  ) => {
    if (checked) {
      dispatch(
        applyProductsFilter({
          ...appliedFilters,
          [type]: [...appliedFilters[type], category.toLowerCase()],
        })
      );
      dispatch(filterProducts());
    }
    if (!checked) {
      dispatch(
        deleteAppliedFilter({ type: type, value: category.toLowerCase() })
      );
      dispatch(filterProducts());
    }
  };
  //---- active accordion ----//
  const activeSection = (index: number) => {
    if (index === section) return true;
    return false;
  };

  return {
    handleSectionClick,
    activeSection,
    handleRootPorductsFilter,
    handleCategoryChecked,
  };
};
