import styled from 'styled-components';
import { DeviceSizes } from '../../../../constants/deviceSizes';

export const SidePanelContainer = styled.aside`
  width: 100%;
  max-width: 290px;
  padding: 10px;
  @media only screen and (max-width: ${DeviceSizes.laptop}px) {
    display: none;
  }
`;

export const BorderDevider = styled.div`
  width: 100%;
  margin: 7px 0px;
  border: ${({ theme }) => `1px solid ${theme.colors.darkerAntiFlashWhite}`};
`;

export const CategoryButton = styled.button`
  all: unset;
  display: block;
  cursor: pointer;
  padding: 0px 8px;
  margin: 15px 0px;
  font-weight: 500;
  &:hover {
    color: ${({ theme }) => theme.colors.cadetGray};
  }
`;
