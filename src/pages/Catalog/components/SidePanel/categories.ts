import { CategoriesData } from './types';

export const categories: CategoriesData[] = [
  {
    title: 'Categories',
    categories: ['Shoes', 'Hats', 'Hoodies', 'Jackets', 'T-Shirts', 'Pants'],
  },
  {
    title: 'Gender',
    categories: ['Men', 'Women'],
  },
  {
    title: 'Size',
    categories: [
      'XS',
      'S',
      'M',
      'L',
      'XL',
      'XXL',
      '37',
      '38',
      '39',
      '40',
      '41',
      '42',
      '43',
      '44',
      '45',
    ],
  },
  {
    title: 'Price',
    categories: ['$0 - $25', '$25 - $50', '$50 - $100', '$100 - $150'],
  },
];
