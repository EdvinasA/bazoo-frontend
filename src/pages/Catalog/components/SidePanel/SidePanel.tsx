import { Fragment } from 'react';
import { categories } from './categories';
import { SidePanelContainer, CategoryButton } from './styles';
import CategoryAccordion from './components/CategoryAccordion/CategoryAccordion';
import { useSidePanel } from './useSidePanel';
import { SpacerWrapper, TextWrapper } from '../../../../styles/wrappers';
import { ProductsFiltersTypes } from '../../../../store/products/types';

const SidePanel = (): JSX.Element => {
  const {
    handleSectionClick,
    activeSection,
    handleRootPorductsFilter,
    handleCategoryChecked,
  } = useSidePanel();

  return (
    <SidePanelContainer>
      {categories.map((item, index) => (
        <Fragment key={index}>
          {item.title === 'Categories' ? (
            <>
              <SpacerWrapper marginLeft='8px'>
                <TextWrapper color='charcoal' fontSize='base'>
                  {item.title}
                </TextWrapper>
              </SpacerWrapper>
              <Fragment>
                {item.categories?.map((category, index) => (
                  <CategoryButton
                    key={index}
                    onClick={() => handleRootPorductsFilter(category)}
                  >
                    {category}
                  </CategoryButton>
                ))}
              </Fragment>
            </>
          ) : (
            <CategoryAccordion
              title={item.title}
              data={item.categories}
              onClick={() => handleSectionClick(index)}
              isActive={activeSection(index)}
              isAccordionOpen={activeSection(index)}
              handleChecked={(checked, category) =>
                handleCategoryChecked(
                  checked,
                  category,
                  item.title.toLowerCase() as ProductsFiltersTypes
                )
              }
            />
          )}
        </Fragment>
      ))}
    </SidePanelContainer>
  );
};

export default SidePanel;
