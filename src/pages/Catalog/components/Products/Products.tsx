import { useNavigate } from 'react-router-dom';
import ProductCard from '../../../../components/ProductCard/ProductCard';
import { ProductCardType } from '../../../../components/ProductCard/types';
import { SpacerWrapper } from '../../../../styles/wrappers';
import AppliedFilters from '../AppliedFilters/AppliedFilters';
import { ProductsContainer } from './styles';
import { useProducts } from './useProducts';

const Products = ({}): JSX.Element => {
  const { productsData } = useProducts();
  const navigate = useNavigate();
  return (
    <SpacerWrapper width='100%'>
      <AppliedFilters />
      <ProductsContainer>
        {productsData().map((item, index) => (
          <ProductCard
            key={index}
            cardType={ProductCardType.Catalog}
            {...item}
            image={item.images[0].src}
            colors={item.colors.length}
            onClick={() => navigate(`/product/${item.slug}`)}
          />
        ))}
      </ProductsContainer>
    </SpacerWrapper>
  );
};

export default Products;
