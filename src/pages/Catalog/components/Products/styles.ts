import styled from 'styled-components';
import { DeviceSizes } from '../../../../constants/deviceSizes';

export const ProductsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  column-gap: 20px;
  row-gap: 20px;
  @media only screen and (max-width: ${DeviceSizes.laptop}px) {
    grid-template-columns: repeat(2, 1fr);
  }
  @media only screen and (max-width: ${DeviceSizes.mobile}px) {
    grid-template-columns: 1fr;
  }
`;
