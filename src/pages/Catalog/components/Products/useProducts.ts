import { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import {
  getProducts,
  getProductFilters,
  getFilteredProducts,
} from '../../../../store/products';
import { getAllProducts } from '../../../../store/products/products.actions';
import { ProductsFilter } from '../../../../store/products/types';

export const useProducts = () => {
  const products = useAppSelector(getProducts);
  const filteredProducts = useAppSelector(getFilteredProducts);
  const appliedFilters = useAppSelector(getProductFilters);
  const dispatch = useAppDispatch();

  // return data
  const productsData = () => {
    for (let key in appliedFilters)
      if (
        appliedFilters.category !== '' ||
        appliedFilters[key as keyof ProductsFilter].length > 0
      )
        return filteredProducts;
    return products;
  };

  //---- useEffect ----//
  useEffect(() => {
    if (!products.length) {
      dispatch(getAllProducts());
    }
  }, [products]);

  return { productsData };
};
