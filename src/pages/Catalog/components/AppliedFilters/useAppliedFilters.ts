import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import {
  getFilteredProducts,
  getProductFilters,
} from '../../../../store/products';
import { deleteAppliedFilter } from '../../../../store/products/products.slice';
import {
  ProductsFilter,
  ProductsFiltersTypes,
} from '../../../../store/products/types';
import { filterProducts } from '../../../../store/products/products.actions';

export const useAppliedFilters = () => {
  const dispatch = useAppDispatch();
  const appliedFilters = useAppSelector(getProductFilters);
  const filteredProducts = useAppSelector(getFilteredProducts);

  //handler
  const handleDeleteFilter = (type: ProductsFiltersTypes, filter: string) => {
    dispatch(deleteAppliedFilter({ type: type, value: filter }));
    dispatch(filterProducts());
  };

  const isAppliedFiltersShowing = () => {
    for (let key in appliedFilters)
      if (
        appliedFilters.category !== '' ||
        appliedFilters[key as keyof ProductsFilter].length > 0
      )
        return true;
    return false;
  };

  return {
    appliedFilters,
    handleDeleteFilter,
    filteredProducts,
    isAppliedFiltersShowing,
  };
};
