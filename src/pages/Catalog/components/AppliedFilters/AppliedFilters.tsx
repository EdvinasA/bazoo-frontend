import {
  FlexWrapper,
  SpacerWrapper,
  TextWrapper,
} from '../../../../styles/wrappers';
import { useAppliedFilters } from './useAppliedFilters';
import FilterButton from './components/FilterButton';
import { ProductsFiltersTypes } from '../../../../store/products/types';

const AppliedFilters = (): JSX.Element => {
  const {
    appliedFilters,
    handleDeleteFilter,
    filteredProducts,
    isAppliedFiltersShowing,
  } = useAppliedFilters();

  return (
    <>
      {isAppliedFiltersShowing() ? (
        <FlexWrapper flexDirection='column' marginBottom='35px' width='100%'>
          <TextWrapper>
            Showing {filteredProducts.length}{' '}
            {filteredProducts.length > 1 ? 'results' : 'result'} from total{' '}
            {filteredProducts.length} for "{appliedFilters.category}"
          </TextWrapper>
          <SpacerWrapper margin='12px 0px' />
          <FlexWrapper alignItems='center' flexWrap='wrap'>
            <TextWrapper>Applied filters:</TextWrapper>
            {appliedFilters.category ? (
              <FilterButton
                title={appliedFilters.category.toUpperCase()}
                onClick={() =>
                  handleDeleteFilter(
                    ProductsFiltersTypes.Category,
                    appliedFilters.category
                  )
                }
              />
            ) : null}
            {appliedFilters.gender.map((item, index) => (
              <FilterButton
                key={index}
                title={item.toUpperCase()}
                onClick={() =>
                  handleDeleteFilter(ProductsFiltersTypes.Gender, item)
                }
              />
            ))}
            {appliedFilters.size.map((item, index) => (
              <FilterButton
                key={index}
                title={item.toUpperCase()}
                onClick={() =>
                  handleDeleteFilter(ProductsFiltersTypes.Size, item)
                }
              />
            ))}
            {appliedFilters.price.map((item, index) => (
              <FilterButton
                key={index}
                title={item.toUpperCase()}
                onClick={() =>
                  handleDeleteFilter(ProductsFiltersTypes.Price, item)
                }
              />
            ))}
          </FlexWrapper>
        </FlexWrapper>
      ) : null}
    </>
  );
};

export default AppliedFilters;
