import styled from 'styled-components';

export const FilterButtonContainer = styled.div`
  margin: 7px;
  height: 38px;
  border: ${({ theme }) => `1px solid ${theme.colors.darkerAntiFlashWhite}`};
  color: ${({ theme }) => theme.colors.charcoal};
  border-radius: 50px;
  display: flex;
  align-items: center;
  padding: 0px 13px;
  gap: 5px;
`;
