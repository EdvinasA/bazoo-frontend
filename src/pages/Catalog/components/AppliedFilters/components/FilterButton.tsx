import { FC } from 'react';
import { FilterButtonContainer } from './styles';
import Button from '../../../../../components/Button/Button';
import { IconNames } from '../../../../../assets/icons';
import { TextWrapper } from '../../../../../styles/wrappers';

interface FilterButtonProps {
  title: string;
  onClick: () => void;
}

const FilterButton: FC<FilterButtonProps> = ({
  title,
  onClick,
}): JSX.Element => {
  return (
    <FilterButtonContainer>
      <TextWrapper>{title}</TextWrapper>
      <Button
        iconSrc={IconNames.CloseIcon}
        iconWidth={25}
        iconHeight={25}
        onClick={onClick}
        width='25px'
      />
    </FilterButtonContainer>
  );
};

export default FilterButton;
