import { useAppSelector } from '../../../../hooks/redux';
import { getTotalPrice } from '../../../../store/cart';

export const useCheckout = () => {
  const totalPrice = useAppSelector(getTotalPrice);
  return { totalPrice };
};
