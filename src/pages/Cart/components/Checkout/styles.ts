import styled from 'styled-components';

export const CheckoutCardContainer = styled.div`
  border: ${({ theme }) => `1px solid ${theme.colors.darkerAntiFlashWhite}`};
  border-radius: 10px;
  width: 100%;
  height: 258px;
  padding: 20px;
`;

export const BorderDevider = styled.div`
  width: 100%;
  margin: 7px 0px;
  border: ${({ theme }) => `1px solid ${theme.colors.darkerAntiFlashWhite}`};
`;
