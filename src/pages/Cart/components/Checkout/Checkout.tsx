import { Icon, IconNames } from '../../../../assets/icons';
import Button from '../../../../components/Button/Button';
import {
  FlexWrapper,
  SpacerWrapper,
  TextWrapper,
} from '../../../../styles/wrappers';
import { CheckoutCardContainer, BorderDevider } from './styles';
import { useCheckout } from './useCheckout';

const Checkout = (): JSX.Element => {
  const { totalPrice } = useCheckout();
  return (
    <CheckoutCardContainer>
      <FlexWrapper justifyContent='space-between' marginBottom='25px'>
        <TextWrapper color='cadetGray'>Subtotal</TextWrapper>
        <FlexWrapper>
          <Icon iconName={IconNames.DollarIcon} width={15} fill='charcoal' />
          <TextWrapper color='charcoal' fontWeight='600'>
            {totalPrice}
          </TextWrapper>
        </FlexWrapper>
      </FlexWrapper>
      <FlexWrapper justifyContent='space-between' margin='25px 0px'>
        <TextWrapper color='cadetGray'>Discount</TextWrapper>
        <FlexWrapper>
          <Icon iconName={IconNames.DollarIcon} width={15} fill='cadetGray' />
          <SpacerWrapper marginLeft='-2px'>
            <TextWrapper color='cadetGray'>0</TextWrapper>
          </SpacerWrapper>
        </FlexWrapper>
      </FlexWrapper>
      <BorderDevider />
      <FlexWrapper justifyContent='space-between' margin='25px 0px'>
        <TextWrapper color='charcoal'>Grand total</TextWrapper>
        <FlexWrapper>
          <Icon iconName={IconNames.DollarIcon} width={20} fill='charcoal' />
          <TextWrapper color='charcoal' fontWeight='700' fontSize='font23'>
            {totalPrice}
          </TextWrapper>
        </FlexWrapper>
      </FlexWrapper>
      <Button
        onClick={() => window.alert('Hello')}
        title='Checkout now'
        width='100%'
        height='45px'
        backgroundColor='gunmetal'
        textColor='white'
      />
    </CheckoutCardContainer>
  );
};

export default Checkout;
