import { FC, Fragment } from 'react';
import { InputNumberActions } from '../../../../components/InputNumber/types';
import DesktopCartItem from './components/DesktopCartItem/DesktopCartItem';
import { CartProduct } from '../../../../store/cart/types';
import CartDetailsRow from './components/CartDetailsRow/CartDetailsRow';
import { BorderDivider } from '../MobileCartView/components/MobileCartItem/styles';

interface DesktopCartViewProps {
  data: CartProduct[];
  updateQuantity: (action: InputNumberActions, index: number) => void;
  deleteProduct: (index: number) => void;
}

const DesktopCartView: FC<DesktopCartViewProps> = ({
  data,
  updateQuantity,
  deleteProduct,
}) => {
  return (
    <>
      <CartDetailsRow />
      <article>
        {data.map((product, index) => (
          <Fragment key={index}>
            <BorderDivider />
            <DesktopCartItem
              {...product}
              updateQuantity={(action) => updateQuantity(action, index)}
              deleteProduct={() => deleteProduct(index)}
            />
          </Fragment>
        ))}
      </article>
    </>
  );
};

export default DesktopCartView;
