import { TextWrapper } from '../../../../../../styles/wrappers';
import { GridContainer } from '../DesktopCartItem/styles';

const data: string[] = ['product', 'quantity', 'price'];

const CartDetailsRow = () => {
  return (
    <GridContainer>
      {data.map((item, index, { length }) => (
        <TextWrapper
          key={index}
          color='cadetGray'
          fontWeight='500'
          textAlign={
            length - 1 === index ? 'right' : index === 1 ? 'center' : 'left'
          }
        >
          {item.toUpperCase()}
        </TextWrapper>
      ))}
    </GridContainer>
  );
};

export default CartDetailsRow;
