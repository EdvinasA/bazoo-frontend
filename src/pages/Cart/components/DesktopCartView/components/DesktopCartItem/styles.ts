import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const GridContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 0.5fr 0.5fr;
`;

export const ImageContainer = styled.div`
  width: 100%;
  max-width: 135px;
  height: 150px;
  border-radius: 10px;
  overflow: hidden;
  margin-right: 18px;
`;

export const ProductLink = styled(Link)`
  all: unset;
  color: ${({ theme }) => theme.colors.charcoal};
  font-size: ${({ theme }) => theme.fontSize.font18};
  font-weight: 700;
  margin-right: 10px;
  cursor: pointer;
`;
