import { InputNumberActions } from '../../../../../../components/InputNumber/types';
import { ReadonlyProps } from '../../../../../../types/types';
import { GridContainer, ImageContainer, ProductLink } from './styles';
import Image from '../../../../../../components/Image/Image';
import {
  FlexWrapper,
  SpacerWrapper,
  TextWrapper,
} from '../../../../../../styles/wrappers';
import InputNumber from '../../../../../../components/InputNumber/InputNumber';
import Button from '../../../../../../components/Button/Button';
import { Icon, IconNames } from '../../../../../../assets/icons';

interface DesktopCartItemProps {
  slug: string;
  image: string;
  title: string;
  color: string;
  size: string;
  price: string;
  quantity: number;
  updateQuantity: (action: InputNumberActions) => void;
  deleteProduct: () => void;
}

type ReadonlyComponentProps = ReadonlyProps<DesktopCartItemProps>;

const DesktopCartItem = (props: ReadonlyComponentProps): JSX.Element => {
  return (
    <article>
      <GridContainer>
        <FlexWrapper>
          <ImageContainer>
            <Image
              width='100%'
              height='100%'
              withEffect
              src={props.image}
              alt={'product-image'}
              style={{ objectFit: 'cover' }}
            />
          </ImageContainer>
          <div>
            <ProductLink to={`/product/${props.slug}`}>
              {props.title}
            </ProductLink>
            <SpacerWrapper margin='15px 0px'>
              <TextWrapper color='cadetGray'>
                {props.color} | {props.size.toUpperCase()}
              </TextWrapper>
            </SpacerWrapper>
          </div>
        </FlexWrapper>
        <FlexWrapper alignItems='center' flexDirection='column'>
          <InputNumber
            value={props.quantity}
            onChange={() => console.log('on mouse wheel change')}
            handleNumberChange={(action) => props.updateQuantity(action)}
          />
          <SpacerWrapper marginBottom='20px' />
          <Button
            title='Remove'
            titleIconLeft={IconNames.TrashIcon}
            textColor='gunmetal'
            width='100px'
            height='22px'
            onClick={() => props.deleteProduct()}
          />
        </FlexWrapper>
        <FlexWrapper flexDirection='column'>
          <FlexWrapper justifyContent='flex-end'>
            <Icon iconName={IconNames.DollarIcon} width={20} />
            <TextWrapper fontSize='font20' fontWeight='600'>
              {props.price}
            </TextWrapper>
          </FlexWrapper>
        </FlexWrapper>
      </GridContainer>
    </article>
  );
};

export default DesktopCartItem;
