import { FC } from 'react';
import MobileCartItem from './components/MobileCartItem/MobileCartItem';
import { CartProduct } from '../../../../store/cart/types';
import { InputNumberActions } from '../../../../components/InputNumber/types';

interface MobileCartViewProps {
  data: CartProduct[];
  updateQuantity: (action: InputNumberActions, index: number) => void;
  deleteProduct: (index: number) => void;
}

const MobileCartView: FC<MobileCartViewProps> = ({
  data,
  updateQuantity,
  deleteProduct,
}) => {
  return (
    <article>
      {data.map((product, index) => (
        <MobileCartItem
          key={index}
          {...product}
          updateQuantity={(action) => updateQuantity(action, index)}
          deleteProduct={() => deleteProduct(index)}
        />
      ))}
    </article>
  );
};

export default MobileCartView;
