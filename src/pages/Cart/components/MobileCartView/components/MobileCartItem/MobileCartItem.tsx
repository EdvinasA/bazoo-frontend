import { ImageContainer, ProductLink, BorderDivider } from './styles';
import Image from '../../../../../../components/Image/Image';
import { ReadonlyProps } from '../../../../../../types/types';
import {
  FlexWrapper,
  SpacerWrapper,
  TextWrapper,
} from '../../../../../../styles/wrappers';
import { Icon, IconNames } from '../../../../../../assets/icons';
import InputNumber from '../../../../../../components/InputNumber/InputNumber';
import { InputNumberActions } from '../../../../../../components/InputNumber/types';
import Button from '../../../../../../components/Button/Button';

interface MobileCartItemProps {
  slug: string;
  image: string;
  title: string;
  color: string;
  size: string;
  price: string;
  quantity: number;
  updateQuantity: (action: InputNumberActions) => void;
  deleteProduct: () => void;
}

type ReadonlyComponentProps = ReadonlyProps<MobileCartItemProps>;

const MobileCartItem = (props: ReadonlyComponentProps): JSX.Element => {
  return (
    <>
      <BorderDivider />
      <FlexWrapper justifyContent='space-between' alignItems='center'>
        <ProductLink to={`/product/${props.slug}`}>{props.title}</ProductLink>
        <Button
          iconSrc={IconNames.TrashIcon}
          iconWidth={30}
          width='30px'
          height='30px'
          onClick={() => props.deleteProduct()}
        />
      </FlexWrapper>
      <FlexWrapper marginTop='20px' width='100%'>
        <ImageContainer>
          <Image
            width='100%'
            height='100%'
            withEffect
            src={props.image}
            alt={'product-image'}
            style={{ objectFit: 'cover' }}
          />
        </ImageContainer>
        <SpacerWrapper width='100%'>
          <SpacerWrapper marginBottom='10px' />
          <TextWrapper color='cadetGray'>
            {props.color} | {props.size.toUpperCase()}
          </TextWrapper>
          <FlexWrapper margin='20px 0px'>
            <Icon iconName={IconNames.DollarIcon} width={20} />
            <TextWrapper>{props.price}</TextWrapper>
          </FlexWrapper>
          <FlexWrapper
            justifyContent='space-between'
            alignItems='center'
            width='100%'
          >
            <InputNumber
              value={props.quantity}
              onChange={() => console.log('on change')}
              handleNumberChange={(action) => props.updateQuantity(action)}
            />
          </FlexWrapper>
        </SpacerWrapper>
      </FlexWrapper>
    </>
  );
};

export default MobileCartItem;
