import { useNavigate } from 'react-router-dom';
import { InputNumberActions } from '../../components/InputNumber/types';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { getCartProducts } from '../../store/cart';
import {
  deleteAllProducts,
  deleteProductFromCart,
  updateProductQuantity,
  updateTotalPriceAndQuantity,
} from '../../store/cart/cart.slice';
import { useEffect, useState } from 'react';
import { DeviceSizes } from '../../constants/deviceSizes';

export const useCart = () => {
  const [windowWidth, setWindowWidth] = useState<number>(window.innerWidth);
  const [isMobileDevice, setIsMobileDevice] = useState<boolean>(
    windowWidth > DeviceSizes.mobile ? false : true
  );
  const cartProducts = useAppSelector(getCartProducts);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  // update quantity handler
  const handleUpdateProductQuantity = (
    index: number,
    action: InputNumberActions
  ) => {
    dispatch(updateProductQuantity({ index: index, action: action }));
    dispatch(updateTotalPriceAndQuantity());
  };
  // delete product handler
  const deleteProductHandler = (index: number) => {
    dispatch(deleteProductFromCart(index));
    dispatch(updateTotalPriceAndQuantity());
  };
  // delete all products
  const deleteAllCartProducts = () => {
    dispatch(deleteAllProducts());
  };
  //---- useEffect ----//
  useEffect(() => {
    const handleWindowWidthChange = () => {
      setWindowWidth(window.innerWidth);
    };
    if (windowWidth < DeviceSizes.mobile) {
      setIsMobileDevice(true);
    }
    if (windowWidth > DeviceSizes.mobile) {
      setIsMobileDevice(false);
    }
    window.addEventListener('resize', handleWindowWidthChange);
    return () => {
      window.removeEventListener('resize', handleWindowWidthChange);
    };
  }, [windowWidth]);

  useEffect(() => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }, []);

  return {
    cartProducts,
    navigate,
    handleUpdateProductQuantity,
    deleteProductHandler,
    deleteAllCartProducts,
    isMobileDevice,
  };
};
