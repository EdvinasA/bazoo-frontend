import { Fragment } from 'react';
import Heading from '../../components/Heading/Heading';
import { FlexWrapper, TextWrapper } from '../../styles/wrappers';
import Checkout from './components/Checkout/Checkout';
import { routePath } from '../../constants/routes';
import Button from '../../components/Button/Button';
import { IconNames } from '../../assets/icons';
import { CartContainer } from './styles';
import { useCart } from './useCart';
import DesktopCartView from './components/DesktopCartView/DesktopCartView';
import MobileCartView from './components/MobileCartView/MobileCartView';

const Cart = (): JSX.Element => {
  const {
    cartProducts,
    navigate,
    handleUpdateProductQuantity,
    deleteProductHandler,
    deleteAllCartProducts,
    isMobileDevice,
  } = useCart();

  return (
    <CartContainer>
      <div>
        <FlexWrapper
          justifyContent='space-between'
          alignItems='center'
          marginBottom='25px'
        >
          <Heading title='Cart' />
          {cartProducts.length > 0 && (
            <Button
              onClick={() => deleteAllCartProducts()}
              title='Remove All'
              titleIconLeft={IconNames.TrashIcon}
              textColor='gunmetal'
              width='130px'
              height='22px'
            />
          )}
        </FlexWrapper>
        {cartProducts.length > 0 ? (
          <Fragment>
            {!isMobileDevice ? (
              <DesktopCartView
                data={cartProducts}
                updateQuantity={(action, index) =>
                  handleUpdateProductQuantity(index, action)
                }
                deleteProduct={(index) => deleteProductHandler(index)}
              />
            ) : (
              <MobileCartView
                data={cartProducts}
                updateQuantity={(action, index) =>
                  handleUpdateProductQuantity(index, action)
                }
                deleteProduct={(index) => deleteProductHandler(index)}
              />
            )}
          </Fragment>
        ) : (
          <FlexWrapper alignItems='center' gap='45px' flexWrap='wrap'>
            <TextWrapper width='100%'>
              You do not have any product in your cart yet.
            </TextWrapper>
            <Button
              title='Go to shop'
              titleIconRight={IconNames.ArrowRight}
              onClick={() => navigate(routePath.catalog)}
              width='165px'
              height='35px'
              isBorder
              textColor='gunmetal'
            />
          </FlexWrapper>
        )}
      </div>
      <Checkout />
    </CartContainer>
  );
};

export default Cart;
