import styled from 'styled-components';
import { DeviceSizes } from '../../constants/deviceSizes';

export const CartContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 0.5fr;
  gap: 50px;
  min-height: 55vh;
  @media only screen and (max-width: ${DeviceSizes.laptop}px) {
    gap: 90px;
    grid-template-columns: 1fr;
  }
`;
